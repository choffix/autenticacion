/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.ejb;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import py.com.konecta.entities.RegistroDeSucesos;

/**
 *
 * @author Kiki
 */
public interface GenericDao<T, ID extends Serializable> {

    public T get(ID id);

    public T getLocked(ID id);

    public T get(T ejemplo);

    public Map<String, Object> getAtributos(T ejemplo, String[] atributos);

    public void save(T entity) throws Exception;

    public void update(T entity,String nombreUsuario,String entidadFinanciera) throws Exception;

    public void delete(ID id,String nombreUsuario,String entidadFinanciera) throws Exception;

    public void delete(T entity,RegistroDeSucesos sucesos) throws Exception;

    public Integer total();

    public Integer total(T ejemplo);

    public Integer total(T ejemplo, boolean like);

    public List<T> list();

    public List<T> list(Integer primerResultado, Integer cantResultado);

    public List<T> list(T ejemplo);

    public List<T> list(T ejemplo, boolean like);

    public List<T> list(T ejemplo, String orderBy, String dir);

    public List<T> list(T ejemplo, String orderBy, String dir, boolean like);

    public List<T> list(T ejemplo, String[] orderBy, String[] dir);

    public List<T> list(T ejemplo, String[] orderBy, String[] dir, boolean like);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, boolean like);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String orderBy, String dir);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String orderBy, String dir, boolean like);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String[] orderBy, String[] dir);

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String[] orderBy, String[] dir, boolean like);

    public List<T> list(T ejemplo, boolean all, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like);
    
    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String orderBy, String dir);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String orderBy, String dir, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String[] orderBy, String[] dir);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String[] orderBy, String[] dir, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String orderBy, String dir);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String orderBy, String dir, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String[] orderBy, String[] dir);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String[] orderBy, String[] dir, boolean like);

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, boolean all, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like);

    public Integer traerUltimaNumeracion(String nombreSec);

    public boolean validarViolacioDeClave(ID codigo);

    public void huellaAuditoria(RegistroDeSucesos registro);

    //public Integer traerUltimoCodigo(String nombreSec);
}

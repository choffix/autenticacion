/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.ejb;

import javax.ejb.Local;
import py.com.konecta.entities.RegistroDeSucesos;

/**
 *
 * @author Kiki
 */
@Local
public interface RegistroSucesoManagerLocal extends GenericDao<RegistroDeSucesos, String> {

}

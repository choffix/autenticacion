/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.ejb;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import py.com.konecta.entities.Usuario;

/**
 *
 * @author desa1
 */
@Local
public interface UsuarioManagerLocal extends GenericDao<Usuario, Integer> {
    public List<Map<String, Object>> listarRoles(Integer codigo);
    public Map<String,Object> listaPermisos(Integer idUsuario);
    public String validarUsuarioPassword(String nombre, String password);
    public Integer traerIdUsuarioxNombreUsuario(String nombreUsuario);
    public Integer cantidadIntentos(String nombreUsuario);
    public String setearCantidadIntentos(String nombreUsuario,Integer cantidad);
    public String intentoFallidoUsuarioInactivo(Integer idUsuario,String estado);
    public List<Map<String, Object>> listarUsuariosPorInstitucion(Integer idUsuario);

}
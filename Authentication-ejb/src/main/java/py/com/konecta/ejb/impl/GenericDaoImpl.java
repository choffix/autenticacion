package py.com.konecta.ejb.impl;

import com.googlecode.genericdao.search.ExampleOptions;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.hibernate.HibernateMetadataUtil;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernateEntityManager;
import py.com.konecta.ejb.GenericDao;
import py.com.konecta.entities.EntidadPadre;
import py.com.konecta.entities.RegistroDeSucesos;

/**
 *
 * @author JLima
 * @param <T> Clase Entidad
 * @param <ID> Clase ID de la Entidad
 */

public abstract class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

    private Class<T> entityBeanType;

    @PersistenceContext
    private EntityManager em;

    public GenericDaoImpl() {
        this.entityBeanType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEm() {
        if (em == null) {
            throw new IllegalStateException("EntityManager no esta seteado");
        }
        return em;
    }

    private SessionFactory getSessionFactory() {
        if (this.em.getDelegate() instanceof HibernateEntityManager) {
            return ((HibernateEntityManager) this.getEm().getDelegate()).getSession().getSessionFactory();
        } else {
            return ((Session) this.getEm().getDelegate()).getSessionFactory();
        }
    }

    private Class<T> getEntityBeanType() {
        return entityBeanType;
    }

    public T get(ID id) {
        return (T) getEm().find(getEntityBeanType(), id);
    }

    public T getLocked(ID id) {
        T t = (T) getEm().getReference(entityBeanType, id);
        getEm().lock(t, LockModeType.WRITE);
        return t;
    }

    public T get(T ejemplo) {
        List<T> list = this.list(ejemplo, 0, 2);

        if (list.size() == 0) {
            return null;
        } else if (list.size() == 1) {
            return list.get(0);
        }

        throw new NonUniqueResultException("Se encontro mas de un "
                + this.getEntityBeanType().getCanonicalName()
                + " para el pedido dado");
    }

    public Map<String, Object> getAtributos(T ejemplo, String[] atributos) {
        List<Map<String, Object>> lista = this.listAtributos(ejemplo, atributos, 0, 2);

        if (lista.size() == 0) {
            return null;
        }

        if (lista.size() == 1) {
            return lista.get(0);
        }

        throw new NonUniqueResultException("Se encontro mas de un "
                + this.getEntityBeanType().getCanonicalName()
                + " para el pedido dado");
    }

    public void save(T entity) throws Exception{

       try{

            //getEm().persist(em);
            this.getEm().persist(entity);
            this.getEm().flush();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("AVISO");
            sucesos.setDescripcion("Se ha guadado una entidad:"+entity.getClass().getName());
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(((EntidadPadre)(entity)).getUsuarioSys());
            sucesos.setIfi(((EntidadPadre)(entity)).getEntidadFinancieraSys());
            huellaAuditoria(sucesos);

            //getEm().persist(em);
        }

        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
            //throw new Exception("Ocurrio un error al guardar la Entidad:"+entity.getClass().getName());
        }

    }


    public void update(T entity,String nombreUsuario,String entidadFinanciera) throws Exception{

        String tipoEntidad = entity.getClass().getName();
        try{

//            HibernateEntityManager hem =
//                    (HibernateEntityManager) getEm().getDelegate();
//            hem.getSession().saveOrUpdate(entity);
//             hem.getSession().flush();
            this.getSession().update(entity);
            this.getSession().merge(entity);
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("AVISO");
            sucesos.setDescripcion("Se ha modificado una entidad:"+tipoEntidad);
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(nombreUsuario);
            sucesos.setIfi(entidadFinanciera);
            huellaAuditoria(sucesos);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrio un error al modificar la Entidad:"+tipoEntidad);
            sucesos.setFecha(Calendar.getInstance().getTime());
           sucesos.setUsuario(nombreUsuario);
            sucesos.setIfi(entidadFinanciera);
            huellaAuditoria(sucesos);
            throw e;
            //throw new Exception("Ocurrio un error al guardar la Entidad:"+entity.getClass().getName());
        }
    }
    public Session getSession(){
        if (this.em.getDelegate() instanceof HibernateEntityManager) { 
            return ((HibernateEntityManager) this.getEm().getDelegate()) 
                    .getSession(); 
        } else { 
            return ((Session) this.getEm().getDelegate()); 
        } 
    }

    public void delete(ID id,String nombreUsuario,String entidadFinanciera) throws Exception{
        try{

           T entity = this.get(id);
           String tipoClase = entity.getClass().getName();
           RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("AVISO");
            sucesos.setDescripcion("Se ha borrado una entidad:"+tipoClase);
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(nombreUsuario);
            sucesos.setIfi(entidadFinanciera);
            this.delete(entity,sucesos);
           // huellaAuditoria(sucesos);
            //getEm().persist(em);
        }
        catch(PersistenceException  e)
        {
            e.printStackTrace();
            if(e.getCause() instanceof org.hibernate.exception.ConstraintViolationException)
                throw new Exception("El registro ya esta relacionada con otra entidad ");
            if(e.getCause() instanceof org.hibernate.exception.GenericJDBCException)
                throw new Exception("Hubo un error al intentar conectarse con la base de datos");
            else
                throw new Exception("Ocurrio un error inesperado en el servidor ");
        }
        catch(Exception e)
        {
          e.printStackTrace();
          throw new Exception("Ocurrio un Error no establecido en el sistema, consulte con su Administrador");
        }


    }

    public void delete(T entity,RegistroDeSucesos sucesos) throws Exception{



        try{
            this.getEm().remove(entity);
            this.getEm().flush();

            huellaAuditoria(sucesos);
            //getEm().persist(em);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
            //throw new Exception("Ocurrio un error al guardar la Entidad:"+entity.getClass().getName());
        }

    }

    public Integer total() {
        return this.total(null, false);
    }

    public Integer total(T ejemplo) {
        return this.total(ejemplo, false);
    }

    public Integer total(T ejemplo, boolean like) {
        JPASearchProcessor jpaSP = new JPASearchProcessor(HibernateMetadataUtil.getInstanceForSessionFactory(this.getSessionFactory()));
        Search searchConfig = this.getSearchConfig(jpaSP, ejemplo, null, true, null, null, null, null, like);
        return jpaSP.count(em, searchConfig);
    }

    public List<T> list() {
        return this.list(null, true, null, null, null, null, false);
    }

    public List<T> list(Integer primerResultado, Integer cantResultado) {
        return this.list(null, false, primerResultado, cantResultado, null, null, false);
    }

    public List<T> list(T ejemplo) {
        return this.list(ejemplo, true, null, null, null, null, false);
    }

    public List<T> list(T ejemplo, boolean like) {
        return this.list(ejemplo, true, null, null, null, null, like);
    }

    public List<T> list(T ejemplo, String orderByAttrList, String orderByDirList) {
        return this.list(ejemplo, true, null, null, new String[]{orderByAttrList}, new String[]{orderByDirList}, false);
    }

    public List<T> list(T ejemplo, String orderByAttrList, String orderByDirList, boolean like) {
        return this.list(ejemplo, true, null, null, new String[]{orderByAttrList}, new String[]{orderByDirList}, like);
    }

    public List<T> list(T ejemplo, String[] orderByAttrList, String[] orderByDirList) {
        return this.list(ejemplo, true, null, null, orderByAttrList, orderByDirList, false);
    }

    public List<T> list(T ejemplo, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        return this.list(ejemplo, true, null, null, orderByAttrList, orderByDirList, like);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados) {
        return this.list(ejemplo, false, primerResultado, cantResultados, null, null, false);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, boolean like) {
        return this.list(ejemplo, false, primerResultado, cantResultados, null, null, like);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String orderByAttrList, String orderByDirList) {
        return this.list(ejemplo, false, primerResultado, cantResultados, new String[]{orderByAttrList}, new String[]{orderByDirList}, false);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String orderByAttrList, String orderByDirList, boolean like) {
        return this.list(ejemplo, false, primerResultado, cantResultados, new String[]{orderByAttrList}, new String[]{orderByDirList}, like);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList) {
        return this.list(ejemplo, false, primerResultado, cantResultados, orderByAttrList, orderByDirList, false);
    }

    public List<T> list(T ejemplo, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        return this.list(ejemplo, false, primerResultado, cantResultados, orderByAttrList, orderByDirList, like);
    }

    public List<T> list(T ejemplo, boolean all, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        JPASearchProcessor jpaSP = new JPASearchProcessor(HibernateMetadataUtil.getInstanceForSessionFactory(this.getSessionFactory()));
        Search searchConfig = this.getSearchConfig(jpaSP, ejemplo, null, all, primerResultado, cantResultados, orderByAttrList, orderByDirList, like);
        return jpaSP.search(em, searchConfig);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos) {
        return this.listAtributos(ejemplo, atributos,
                true, null, null,
                null, null, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, boolean like) {
        return this.listAtributos(ejemplo, atributos, true, null, null, null, null, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, null, null, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, boolean like) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, null, null, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String orderByAttrList, String orderByDirList) {
        return this.listAtributos(ejemplo, atributos, true, null, null, new String[]{orderByAttrList}, new String[]{orderByDirList}, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String orderByAttrList, String orderByDirList, boolean like) {
        return this.listAtributos(ejemplo, atributos, true, null, null, new String[]{orderByAttrList}, new String[]{orderByDirList}, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String[] orderByAttrList, String[] orderByDirList) {
        return this.listAtributos(ejemplo, atributos, true, null, null, orderByAttrList, orderByDirList, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        return this.listAtributos(ejemplo, atributos, true, null, null, orderByAttrList, orderByDirList, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String orderByAttrList, String orderByDirList) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, new String[]{orderByAttrList}, new String[]{orderByDirList}, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String orderByAttrList, String orderByDirList, boolean like) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, new String[]{orderByAttrList}, new String[]{orderByDirList}, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, orderByAttrList, orderByDirList, false);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        return this.listAtributos(ejemplo, atributos, false, primerResultado, cantResultados, orderByAttrList, orderByDirList, like);
    }

    public List<Map<String, Object>> listAtributos(T ejemplo, String[] atributos, boolean all, Integer primerResultado, Integer cantResultados, String[] orderByAttrList, String[] orderByDirList, boolean like) {
        if (atributos== null || atributos.length == 0) {
            throw new RuntimeException("La lista de propiedades no puede ser nula o vacía");
        }

        JPASearchProcessor jpaSP = new JPASearchProcessor(HibernateMetadataUtil.getInstanceForSessionFactory(this.getSessionFactory()));
        Search searchConfig = this.getSearchConfig(jpaSP, ejemplo, atributos, all, primerResultado, cantResultados, orderByAttrList, orderByDirList, like);
        return jpaSP.search(em, searchConfig);
    }

    private Search getSearchConfig(JPASearchProcessor jpaSP, T ejemplo, String [] atributos, boolean all, Integer primerResultado, Integer cantResultados,
            String[] orderByAttrList, String[] orderByDirList, boolean like) {


        Search searchConfig = new Search(this.getEntityBeanType());

        if (ejemplo != null) {
            ExampleOptions exampleOptions = new ExampleOptions();
            exampleOptions.setExcludeNulls(true);

            if (like) {
                exampleOptions.setIgnoreCase(true);
                exampleOptions.setLikeMode(ExampleOptions.ANYWHERE);
            }

            searchConfig.addFilter(jpaSP.getFilterFromExample(ejemplo, exampleOptions));
        }

        if (!all) {
            searchConfig.setFirstResult(primerResultado);
            searchConfig.setMaxResults(cantResultados);
        }

        if (orderByAttrList != null && orderByDirList != null && orderByAttrList.length == orderByDirList.length) {
            for (int i = 0; i < orderByAttrList.length; i++) {
                if (orderByDirList[i].equalsIgnoreCase("desc")) {
                    searchConfig.addSortDesc(orderByAttrList[i]);
                } else {
                    searchConfig.addSortAsc(orderByAttrList[i]);
                }
            }
        } else if ((orderByAttrList != null && orderByDirList == null) || (orderByAttrList == null && orderByDirList != null)) {
            throw new RuntimeException("No puede proporcionarse una lista de " +
                    "atributos para ordenamiento sin la correspondiente " +
                    "lista de direcciones de ordenamiento, o viceversa");
        } else if (orderByAttrList != null && orderByDirList != null && orderByAttrList.length != orderByDirList.length) {
            throw new RuntimeException("No puede proporcionarse una lista de " +
                    "atributos para ordenamiento de tamaño dieferente a la " +
                    "lista de direcciones de ordenamiento");
        }


        if (atributos != null && atributos.length > 0) {
            for (String a : atributos) {
                searchConfig.addField(a);
            }
            searchConfig.setResultMode(Search.RESULT_MAP);
        }

        return searchConfig;
    }

    @Override
    public Integer traerUltimaNumeracion(String nombreSec) {
        Query q = getEm().createQuery("select  num.valor "
                + "from Numerador num where num.nombreSec= ?");

        q.setParameter(1, nombreSec);
        List<Integer> resultList = q.getResultList();


        Integer numero = null;
        for (Integer itemList : resultList) {
             numero= itemList;

        }

        return numero;
    }

    //@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void huellaAuditoria(RegistroDeSucesos registro)
    {
        try
        {
            getEm().persist(registro);
            this.getEm().flush();

        }
        catch(Exception e)
        {
            ;
        }
    }

    public boolean validarViolacioDeClave(ID codigo)
    {
        try
        {
             T entity = this.get(codigo);
             if(entity==null)
             {
                 return false;
             }
             else
                return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    /*@Override
    public Integer traerUltimoCodigo(String nombreSec) {
        Query q = getEm().createQuery("select  codgtia.contLetra "
                + "from codigoGarantia codgtia where codgtia.codigo= ?");

        q.setParameter(1, nombreSec);
        List<Integer> resultList = q.getResultList();


        Integer numero = null;
        for (Integer itemList : resultList) {
             numero= itemList;

        }

        return numero;
    }*/
}

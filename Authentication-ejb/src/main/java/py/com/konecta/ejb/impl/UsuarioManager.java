/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.ejb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.Query;
import py.com.konecta.ejb.UsuarioManagerLocal;
import py.com.konecta.ejb.utils.Log;
import py.com.konecta.entities.Usuario;


/**
 *
 * @author desa1
 */


@Stateless
public class UsuarioManager extends GenericDaoImpl<Usuario, Integer> implements UsuarioManagerLocal {

    Log log = new Log();
    
    @Override
    public List<Map<String, Object>> listarRoles(Integer codigo) {
        Query q = getEm().createQuery("select rol.codRol, rol.nombre, rol.delegable "
                + "from Usuario usuario join usuario.roles rol where usuario.idUsuario = ?");

        q.setParameter(1, codigo);

        List<Object[]> resultList = q.getResultList();

        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        for (Object[] itemList : resultList) {
            HashMap<String, Object> itemMap = new HashMap<String, Object>();
            itemMap.put("codRol", itemList[0]);
            itemMap.put("nombre", itemList[1]);
            itemMap.put("delegable", itemList[2]);
            result.add(itemMap);
        }
        log.logFormateado("listarRoles", "codigo:" + codigo.toString(), "Obtiene los roles de un usuario", Boolean.TRUE, "Se otuvo los roles del usuario exitosamente.", "Usuario");
        return result;
    }

    public Map<String,Object> listaPermisos(Integer idUsuario)
    {
        String jpql = "select permiso.codPermiso from Usuario usuario "
                + "join usuario.roles rol "
                + "join rol.permisos  permiso where usuario.idUsuario = ?";
        Query query = getEm().createQuery(jpql);
        query.setParameter(1, idUsuario);
        List<String> resultList = query.getResultList();
        Map<String,Object> result = new HashMap<String, Object>();
        Iterator<String> ite = resultList.iterator();
        String permiso;
        while(ite.hasNext())
        {
            permiso = ite.next();
            result.put(permiso,permiso);
        }
        log.logFormateado("listaPermisos", "idUsuario:" + idUsuario.toString(), "Obtiene la lista de permisos de un Usuario", Boolean.TRUE, "Se otuvo los permisos del usuario exitosamente.", "Usuario");
        return result;
    }

    @Override
    public String validarUsuarioPassword(String nombre, String password) {
        String mismoUsuarioPassword = null;
        if(usuarioValido(nombre) == true){
            if(passwordValido(password) == true){
                if(mismoUsuarioPassword(nombre, password) == true){
                    mismoUsuarioPassword = "IGUAL";
                }else{
                    //En caso de password incorrecto pero usuario correcto
                    //O en cado de usuario incorrecto y password correcto 
                    mismoUsuarioPassword = "PASSWORD";
                }
            }else{
                mismoUsuarioPassword = "PASSWORD";
            }
        }else{
            mismoUsuarioPassword = "USUARIO";
        }
        log.logFormateado("validarUsuarioPassword", "nombre:" + nombre+", password: ******", "Valida el nombre de usuario y su contraseña", Boolean.TRUE, "Validación de Usuario y contraseña realizada exitosamente.", nombre);
        return mismoUsuarioPassword;
    }
    
    private boolean mismoUsuarioPassword(String nombre, String password){
        Query q = getEm().createNativeQuery("select u.nombre_usuario, u.contrasenha from usuario u where u.nombre_usuario = :nombre and u.contrasenha = :pass");
        q.setParameter("nombre", nombre);
        q.setParameter("pass", password);
        if(q.getResultList().size() > 0){
            return true;
        }
        log.logFormateado("mismoUsuarioPassword", "nombre:" + nombre+", password: ******", "Valida si existe otro usuario con el mismo nombre de usuario y password", Boolean.TRUE, "Validación realizada exitosamente.", nombre);
        return false;
    }
    

    private boolean usuarioValido(String nombre){
        Query q = getEm().createNativeQuery("select u.nombre_usuario from usuario u where u.nombre_usuario = :nombre");
        q.setParameter("nombre", nombre);
        if(q.getResultList().size() > 0){
            return true;
        }
        log.logFormateado("usuarioValido", "nombre:" + nombre, "Valida si existe otro usuario con el mismo nombre de usuario", Boolean.TRUE, "Validación realizada exitosamente.", nombre);
        return false;
    }
    
    private boolean passwordValido(String password){
        Query q = getEm().createNativeQuery("select u.contrasenha from usuario u where u.contrasenha = :pass");
        q.setParameter("pass", password);
        if(q.getResultList().size() > 0){
            return true;
        }
        log.logFormateado("passwordValido", "Password: *****", "Valida si existe otro usuario con la misma contraseña", Boolean.TRUE, "Validación realizada exitosamente.", "Usuario");
        return false;
    }
    
    public Integer traerIdUsuarioxNombreUsuario(String nombreUsuario){
        Query q = getEm().createNativeQuery("SELECT u.id_usuario from usuario u where  u.nombres+u.apellidos=?");
        q.setParameter(1, nombreUsuario);
        Integer id = Integer.parseInt(q.getSingleResult().toString());
        return id;
    }

    @Override
    public Integer cantidadIntentos(String nombreUsuario) {
        Query q = getEm().createNativeQuery("SELECT intento_fallido FROM usuario WHERE nombre_usuario = ?");
        q.setParameter(1,nombreUsuario);
        Object objecto = q.getSingleResult();
        Integer cantidad = null;
        if(objecto == null){
            cantidad = 0;
        }else{
            cantidad = Integer.parseInt(q.getSingleResult().toString());
        }
        
        log.logFormateado("cantidadIntentos", "nombreUsuario:" + nombreUsuario, "Verifica la cantidad de veces que se ha fallado al ingresar contraseña", 
                Boolean.TRUE, "Consulta de cantidad de intentos fallidos exitosa",nombreUsuario);
        return cantidad;
    }

    @Override
    public String setearCantidadIntentos(String nombreUsuario, Integer cantidad) {
        try {
            Query q = getEm().createNativeQuery("UPDATE usuario "
                    + "SET intento_fallido=:cantidad "
                    + "WHERE nombre_usuario=:nombreUsuario");

            q.setParameter("cantidad", cantidad);
            q.setParameter("nombreUsuario", nombreUsuario);
            q.executeUpdate();
            
            log.logFormateado("setearCantidadIntentos", "nombreUsuario:" + nombreUsuario + " cantidad: "+String.valueOf(cantidad), "Modificar la cantidad de intentos fallidos de logueo", 
                Boolean.TRUE, "Modificación exitosa",nombreUsuario);
            return "sucess";
        } catch (Exception e) {
            log.logFormateado("setearCantidadIntentos", "nombreUsuario:" + nombreUsuario + " cantidad: "+String.valueOf(cantidad), "Modificar la cantidad de intentos fallidos de logueo", 
                Boolean.FALSE, "Modificación fallida. "+e.getMessage(),nombreUsuario);
            return null;
        }
    }

    @Override
    public String intentoFallidoUsuarioInactivo(Integer idUsuario, String estado) {
        try {
            Query q = getEm().createNativeQuery("UPDATE usuario "
                    + "SET cod_est_usu=:estado "
                    + "WHERE id_usuario=:idUsuario");

            q.setParameter("idUsuario", idUsuario);
            q.setParameter("estado", estado);
            q.executeUpdate();
            
            log.logFormateado("intentoFallidoUsuarioInactivo", "idUsuario:" +String.valueOf(idUsuario) + " estado: "+estado, "Modificar estado de usuario por intentos fallidos de logueo", 
                Boolean.TRUE, "Modificación exitosa","Usuario");
            return "sucess";
        } catch (Exception e) {
            log.logFormateado("intentoFallidoUsuarioInactivo", "idUsuario:" +String.valueOf(idUsuario) + " estado: "+estado, "Modificar estado de usuario por intentos fallidos de logueo", 
                Boolean.FALSE, "Modificación fallida. "+e.getMessage(),"Usuario");
            return null;
        }
    }

    @Override
    public List<Map<String, Object>> listarUsuariosPorInstitucion(Integer idUsuario) {
        Integer esAdminFondo = verificarAdminFondo(idUsuario);
        if(esAdminFondo == 1){
            return obtenerTodosLosUsuarios();
        }else{
            return obtenerUsuariosInstitucion(idUsuario);
        }
    }
    
    private List<Map<String, Object>> obtenerTodosLosUsuarios(){
        Query q = getEm().createNativeQuery("select nombre_usuario, nombres, apellidos, nro_documento, id_sucursal, cod_est_usu, idifi, cod_documento, telefono, email, adminifi, id_usuario, contrasenha, fecha_ultimo_ingreso, cambiarpassword, intento_fallido, passwordpermanente, creado_por, fecha_modificacion, modificado_por from usuario");
        
        List<Object[]> resultList = q.getResultList();

        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        for (Object[] itemList : resultList) {
            HashMap<String, Object> itemMap = new HashMap<String, Object>();
            itemMap.put("nombreUsuario", itemList[0]);
            itemMap.put("nombres", itemList[1]);
            itemMap.put("apellidos", itemList[2]);
            itemMap.put("nroDocumento", itemList[3]);
            if(itemList[4] != null){
                itemMap.put("sucursal", obtenerNombreSucursal(Integer.parseInt(itemList[4].toString())));
            }
            if(itemList[5] != null){
                itemMap.put("estado", obtenerNombreEstadoUsuario(itemList[5].toString()));
            }
            if(itemList[6] != null){
                itemMap.put("ifi", obtenerNombreInstitucion(Integer.parseInt(itemList[6].toString())));
            }
            if(itemList[7] != null){
                itemMap.put("codDocumento", itemList[7].toString());
            }
            if(itemList[8] != null){
                itemMap.put("telefono", itemList[8].toString());
            }
            if(itemList[9] != null){
                itemMap.put("email", itemList[9].toString());
            }
            if(itemList[10] != null){
                itemMap.put("adminifi", itemList[10].toString());
            }
            if(itemList[11] != null){
                itemMap.put("idUsuario", itemList[11].toString());
            }
            if(itemList[12] != null){
                itemMap.put("contrasenha", itemList[12].toString());
            }
            if(itemList[13] != null){
                itemMap.put("fechaUltimoIngreso", itemList[13].toString());
            }
            if(itemList[14] != null){
                itemMap.put("cambiarpassword", itemList[14].toString());
            }
            if(itemList[15] != null){
                itemMap.put("intentoFallido", itemList[15].toString());
            }
            if(itemList[16] != null){
                itemMap.put("passwordpermanente", itemList[16].toString());
            }
            if(itemList[17] != null){
                itemMap.put("creadoPor", itemList[17].toString());
            }
            if(itemList[18] != null){
                itemMap.put("fechaModificacion", itemList[18].toString());
            }
            if(itemList[19] != null){
                itemMap.put("modificadoPor", itemList[19].toString());
            }
            itemMap.put("idIfi", itemList[6]);
            itemMap.put("codEstUsu", itemList[5].toString());
            itemMap.put("codSucursal", itemList[4]);
            result.add(itemMap);
        }
        log.logFormateado("obtenerTodosLosUsuarios", "No se recibieron parámetros", "Obtiene la lista de Usuarios", Boolean.TRUE, "Se obtuvo la lista de los Usuarios exitosamente.", "Usuario");

        return result;
    }
    
    private List<Map<String, Object>> obtenerUsuariosInstitucion(Integer idUsuario) {
        Query q = getEm().createNativeQuery("select nombre_usuario, nombres, apellidos, nro_documento, id_sucursal, cod_est_usu, idifi, cod_documento, telefono, email, adminifi, id_usuario, contrasenha, fecha_ultimo_ingreso, cambiarpassword, intento_fallido, passwordpermanente, creado_por, fecha_modificacion, modificado_por from usuario where creado_por = (select nombre_usuario from dbo.usuario where id_usuario = :idUsuario)");
        q.setParameter("idUsuario", idUsuario);

        List<Object[]> resultList = q.getResultList();
        Integer idIfi = null;

        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        if (resultList.size() > 0) {
            for (Object[] itemList : resultList) {
                HashMap<String, Object> itemMap = new HashMap<String, Object>();
                itemMap.put("nombreUsuario", itemList[0]);
                itemMap.put("nombres", itemList[1]);
                itemMap.put("apellidos", itemList[2]);
                itemMap.put("nroDocumento", itemList[3]);
                if (itemList[4] != null) {
                    itemMap.put("sucursal", obtenerNombreSucursal(Integer.parseInt(itemList[4].toString())));
                }
                if (itemList[5] != null) {
                    itemMap.put("estado", obtenerNombreEstadoUsuario(itemList[5].toString()));
                }
                if (itemList[6] != null) {
                    itemMap.put("ifi", obtenerNombreInstitucion(Integer.parseInt(itemList[6].toString())));
                }
                if (itemList[7] != null) {
                    itemMap.put("codDocumento", itemList[7].toString());
                }
                if (itemList[8] != null) {
                    itemMap.put("telefono", itemList[8].toString());
                }
                if (itemList[9] != null) {
                    itemMap.put("email", itemList[9].toString());
                }
                if (itemList[10] != null) {
                    itemMap.put("adminifi", itemList[10].toString());
                }
                if (itemList[11] != null) {
                    itemMap.put("idUsuario", itemList[11].toString());
                }
                if (itemList[12] != null) {
                    itemMap.put("contrasenha", itemList[12].toString());
                }
                if (itemList[13] != null) {
                    itemMap.put("fechaUltimoIngreso", itemList[13].toString());
                }
                if (itemList[14] != null) {
                    itemMap.put("cambiarpassword", itemList[14].toString());
                }
                if (itemList[15] != null) {
                    itemMap.put("intentoFallido", itemList[15].toString());
                }
                if (itemList[16] != null) {
                    itemMap.put("passwordpermanente", itemList[16].toString());
                }
                if (itemList[17] != null) {
                    itemMap.put("creadoPor", itemList[17].toString());
                }
                if (itemList[18] != null) {
                    itemMap.put("fechaModificacion", itemList[18].toString());
                }
                if (itemList[19] != null) {
                    itemMap.put("modificadoPor", itemList[19].toString());
                }
                itemMap.put("idIfi", itemList[6]);
                itemMap.put("codEstUsu", itemList[5].toString());
                itemMap.put("codSucursal", itemList[4]);
                result.add(itemMap);
            }
        } else {
            Query ifi = getEm().createNativeQuery("select id_usuario, nombre_usuario, nombres, apellidos, nro_documento, id_sucursal, cod_est_usu, idifi, cod_documento, telefono, email, adminifi, contrasenha, fecha_ultimo_ingreso, cambiarpassword, intento_fallido, passwordpermanente, creado_por, fecha_modificacion, modificado_por from usuario");
            List<Object[]> resultList1 = ifi.getResultList();
            if (ifi.getResultList().size() > 0) {
                for (Object[] itemList : resultList1) {
                    HashMap<String, Object> itemMap = new HashMap<String, Object>();
                    Integer valor = Integer.parseInt(itemList[0].toString());

                    if (valor == idUsuario) {
                        if (itemList[7] != null) {
                            idIfi = Integer.parseInt(itemList[7].toString());
                        }
                    }
                }
                for (Object[] itemList : resultList1) {
                    HashMap<String, Object> itemMap = new HashMap<String, Object>();
                    Integer valor = Integer.parseInt(itemList[0].toString());
                    if (valor != idUsuario) {
                        if (itemList[7] != null) {
                            if (idIfi != null && idIfi == Integer.parseInt(itemList[7].toString())) {
                                itemMap.put("ifi", obtenerNombreInstitucion(Integer.parseInt(itemList[7].toString())));
                                itemMap.put("nombreUsuario", itemList[1]);
                                itemMap.put("nombres", itemList[2]);
                                itemMap.put("apellidos", itemList[3]);
                                itemMap.put("nroDocumento", itemList[4]);
                                if (itemList[5] != null) {
                                    itemMap.put("sucursal", obtenerNombreSucursal(Integer.parseInt(itemList[5].toString())));
                                }
                                if (itemList[6] != null) {
                                    itemMap.put("estado", obtenerNombreEstadoUsuario(itemList[6].toString()));
                                }
                                if (itemList[8] != null) {
                                    itemMap.put("codDocumento", itemList[8].toString());
                                }
                                if (itemList[9] != null) {
                                    itemMap.put("telefono", itemList[9].toString());
                                }
                                if (itemList[10] != null) {
                                    itemMap.put("email", itemList[10].toString());
                                }
                                if (itemList[11] != null) {
                                    itemMap.put("adminifi", itemList[11].toString());
                                }
                                if (itemList[0] != null) {
                                    itemMap.put("idUsuario", itemList[0].toString());
                                }
                                if (itemList[12] != null) {
                                    itemMap.put("contrasenha", itemList[12].toString());
                                }
                                if (itemList[13] != null) {
                                    itemMap.put("fechaUltimoIngreso", itemList[13].toString());
                                }
                                if (itemList[14] != null) {
                                    itemMap.put("cambiarpassword", itemList[14].toString());
                                }
                                if (itemList[15] != null) {
                                    itemMap.put("intentoFallido", itemList[15].toString());
                                }
                                if (itemList[16] != null) {
                                    itemMap.put("passwordpermanente", itemList[16].toString());
                                }
                                if (itemList[17] != null) {
                                    itemMap.put("creadoPor", itemList[17].toString());
                                }
                                if (itemList[18] != null) {
                                    itemMap.put("fechaModificacion", itemList[18].toString());
                                }
                                if (itemList[19] != null) {
                                    itemMap.put("modificadoPor", itemList[19].toString());
                                }
                                itemMap.put("idIfi", itemList[7]);
                                itemMap.put("codEstUsu", itemList[6].toString());
                                itemMap.put("codSucursal", itemList[5]);
                            }
                        }else{
                            if(idIfi == null){
                                itemMap.put("ifi", "");
                                itemMap.put("nombreUsuario", itemList[1]);
                                itemMap.put("nombres", itemList[2]);
                                itemMap.put("apellidos", itemList[3]);
                                itemMap.put("nroDocumento", itemList[4]);
                                if (itemList[5] != null) {
                                    itemMap.put("sucursal", obtenerNombreSucursal(Integer.parseInt(itemList[5].toString())));
                                }
                                if (itemList[6] != null) {
                                    itemMap.put("estado", obtenerNombreEstadoUsuario(itemList[6].toString()));
                                }
                                if (itemList[8] != null) {
                                    itemMap.put("codDocumento", itemList[8].toString());
                                }
                                if (itemList[9] != null) {
                                    itemMap.put("telefono", itemList[9].toString());
                                }
                                if (itemList[10] != null) {
                                    itemMap.put("email", itemList[10].toString());
                                }
                                if (itemList[11] != null) {
                                    itemMap.put("adminifi", itemList[11].toString());
                                }
                                if (itemList[0] != null) {
                                    itemMap.put("idUsuario", itemList[0].toString());
                                }
                                if (itemList[12] != null) {
                                    itemMap.put("contrasenha", itemList[12].toString());
                                }
                                if (itemList[13] != null) {
                                    itemMap.put("fechaUltimoIngreso", itemList[13].toString());
                                }
                                if (itemList[14] != null) {
                                    itemMap.put("cambiarpassword", itemList[14].toString());
                                }
                                if (itemList[15] != null) {
                                    itemMap.put("intentoFallido", itemList[15].toString());
                                }
                                if (itemList[16] != null) {
                                    itemMap.put("passwordpermanente", itemList[16].toString());
                                }
                                if (itemList[17] != null) {
                                    itemMap.put("creadoPor", itemList[17].toString());
                                }
                                if (itemList[18] != null) {
                                    itemMap.put("fechaModificacion", itemList[18].toString());
                                }
                                if (itemList[19] != null) {
                                    itemMap.put("modificadoPor", itemList[19].toString());
                                }
                                itemMap.put("idIfi", 0);
                                itemMap.put("codEstUsu", itemList[6].toString());
                                itemMap.put("codSucursal", itemList[5]);
                            }
                        }
                        if(itemMap.size() > 0){
                            result.add(itemMap);
                        }
                        
                    }
                }
            }
        }
        
        log.logFormateado("obtenerUsuariosInstitucion", "idUsuario: " + idUsuario.toString(), "Obtiene la lista de Usuarios por Institución", Boolean.TRUE, "Se obtuvo la lista de los Usuarios exitosamente.", "Usuario");

        return result;
    }
    
    private String obtenerNombreSucursal(Integer idSucursal){
        String nombre = "";
        Query q = getEm().createNativeQuery("select nombre from sucursal where id_sucursal = :idSucursal");
        q.setParameter("idSucursal", idSucursal);
        if(q.getResultList().size() > 0){
            nombre = q.getResultList().get(0).toString();
        }
        log.logFormateado("obtenerNombreSucursal", "idSucursal: " + idSucursal.toString(), "Obtiene el nombre de una sucursal", Boolean.TRUE, "Se obtuvo el nombre de la sucursal exitosamente.", "Usuario");

        return nombre;
    }
    
    private String obtenerNombreEstadoUsuario(String idEstado){
        String nombre = "";
        Query q = getEm().createNativeQuery("select nombre from estado_usuario where cod_est_usu = :idEstado");
        q.setParameter("idEstado", idEstado);
        if(q.getResultList().size() > 0){
            nombre = q.getResultList().get(0).toString();
        }
        log.logFormateado("obtenerNombreEstadoUsuario", "idEstado: " + idEstado, "Obtiene la descripción del estado de un usuario", Boolean.TRUE, "Se obtuvo la descripción del estado del usuario exitosamente.", "Usuario");

        return nombre;
    }
    
    private String obtenerNombreInstitucion(Integer idIfi){
        String nombre = "";
        Query q = getEm().createNativeQuery("select nombre from ifi where idifi = :idIfi");
        q.setParameter("idIfi", idIfi);
        if(q.getResultList().size() > 0){
            nombre = q.getResultList().get(0).toString();
        }
        log.logFormateado("obtenerNombreInstitucion", "idIfi: " + idIfi.toString(), "Obtiene el nombre de una Institución Participante", Boolean.TRUE, "Se obtuvo el nombre de la Institución Participante exitosamente.", "Usuario");

        return nombre;
    }

    private Integer verificarAdminFondo(Integer idUsuario) {
        // 1 si es admin de fondo
        // 2 si es usuario de fondo
        // 3 si es admin de ifi
        // 4 si es usuario de ifi
        Integer valor = 0;
        boolean tieneIfi =  false;
        Query q = getEm().createNativeQuery("select adminifi from usuario where id_usuario = :idUsuario");
        q.setParameter("idUsuario", idUsuario);
        if(q.getResultList().size() > 0){
            boolean valorAdmin = Boolean.parseBoolean(q.getResultList().get(0).toString());
            tieneIfi = verificarIfi(idUsuario);
            if(tieneIfi){
               if(valorAdmin){
                   valor = 3;
               }else{
                   valor = 4;
               }
            }else{
                if(valorAdmin){
                    valor = 1;
                }else{
                    valor = 2;
                }
            }
        }
        log.logFormateado("verificarAdminFondo", "idUsuario: " + idUsuario.toString(), "Verifica si el usuario es administrador", Boolean.TRUE, "Se obtuvo exitosamente la información del usuario.", "Usuario");

        return valor;
    }
    
    private boolean verificarIfi(Integer idUsuario){
        boolean tieneIfi = false;
        Query q = getEm().createNativeQuery("select idifi from usuario where id_usuario = :idUsuario");
        q.setParameter("idUsuario", idUsuario);
        if(q.getResultList().size() > 0){
            if (q.getResultList().get(0) != null) {
                Integer valor = Integer.parseInt(q.getResultList().get(0).toString());
                if (valor != null) {
                    tieneIfi = true;
                }
            }

        }
        log.logFormateado("verificarIfi", "idUsuario: " + idUsuario.toString(), "Verifica si el usuario está asociada a una Institución Participante", Boolean.TRUE, "Se obtuvo exitosamente la información del usuario.", "Usuario");

        return tieneIfi;
    }
    
}

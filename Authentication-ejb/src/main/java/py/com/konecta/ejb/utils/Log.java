/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.ejb.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author konecta
 */
public class Log {
    private Logger logger = LoggerFactory.getLogger(Log.class);

    public Log() {
    }
    
    public void logFormateado(String metodo, String datosEntrada, 
            String accion, Boolean error, String datosSalida, String usuario){
        if(error == false){
            logger.error("METODO=" + metodo + ";" + "ENTRADA=" + datosEntrada + ";" + 
                    "ACCION=" + accion + ";" + "SALIDA=" + datosSalida + ";" + 
                    "USUARIO=" + usuario);
        }else{
            logger.info("METODO=" + metodo + ";" + "ENTRADA=" + datosEntrada + ";" + 
                    "ACCION=" + accion + ";" + "SALIDA=" + datosSalida + ";" + 
                    "USUARIO=" + usuario);
        }
    }
}

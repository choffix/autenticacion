/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author desa1
 */


@Entity
@Table(name="CIUDAD")
public class Ciudad extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_CIUDAD", length=4, nullable=false)
    private String codCiudad;

    @Column(name="NOMBRE", length=100, nullable=false)
    private String nombreCiudad;

    @ManyToOne
    @JoinColumn(name = "COD_DPTO", referencedColumnName = "COD_DPTO")
    private DepartamentoGeografico departamento;

    public Ciudad() {
    }

    public Ciudad(String cod) {
        this.codCiudad = cod;
    }

    public String getCodCiudad() {
        return codCiudad;
    }

    public void setCodCiudad(String codCiudad) {
        this.codCiudad = codCiudad;
    }

    public DepartamentoGeografico getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoGeografico departamento) {
        this.departamento = departamento;
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCiudad != null ? codCiudad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Ciudad)) {
            return false;
        }
        Ciudad other = (Ciudad) object;
        if ((this.codCiudad == null && other.codCiudad != null) ||
        (this.codCiudad != null && !this.codCiudad.equals(other.codCiudad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CODIGO " + "[" + codCiudad + "]";
    }

}

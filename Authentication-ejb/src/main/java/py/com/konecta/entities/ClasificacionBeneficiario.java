/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

/**
 *
 * @author desa1
 */
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


/**
 *
 * @author Kiki
 */
@Entity
@Table(name="CLASIFICACIONBENEFICIARIO")
public class ClasificacionBeneficiario extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @Column(name = "ID_CLASIFICACION", nullable = false)
    @GeneratedValue(generator = "clasificacionBeneficiarioSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "clasificacionBeneficiarioSeq", table = "SECUENCIA", pkColumnName = "NOMBRE_SEC",
    valueColumnName = "VALOR", pkColumnValue = "CLASIFICACIONBENEFICIARIO_SEQ", allocationSize = 1)
    private Integer idClasificacion;


    @Column(name = "COD_CLASIFICACION", nullable = false)
    private String codClasificacion;
    
    @Column(name="NOMBRE_CLASIFICACION", length=100, nullable=false, unique=true)
    private String nombreClasificacion;

    @Column(name="VENTAS_ANUALES",precision=17,scale=2,nullable=true)
    private BigDecimal ventasAnuales;

    @Column(name="NRO_EMPLEADOS",precision=5,nullable=true)
    private Integer nroEmpleados;


    public ClasificacionBeneficiario() {
    }

    public ClasificacionBeneficiario(Integer codClasif) {
        this.idClasificacion = codClasif;
    }

    public Integer getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(Integer idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getNombreClasificacion() {
        return this.nombreClasificacion;
    }

    public void setNombreClasificacion(String nombreClasif) {
        this.nombreClasificacion = nombreClasif;
    }

    public String getCodClasificacion() {
        return this.codClasificacion;
    }

    public void setCodClasificacion(String codigo) {
        this.codClasificacion = codigo;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codClasificacion != null ? codClasificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof ClasificacionBeneficiario)) {
            return false;
        }
        ClasificacionBeneficiario other = (ClasificacionBeneficiario) object;
        if ((this.codClasificacion.toString() == null && other.codClasificacion.toString() != null) ||
        (this.codClasificacion != null && !this.codClasificacion.equals(other.codClasificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codClasificacion.toString();
    }

    public Integer getNroEmpleados() {
        return nroEmpleados;
    }

    public void setNroEmpleados(Integer nroEmpleados) {
        this.nroEmpleados = nroEmpleados;
    }

    public BigDecimal getVentasAnuales() {
        return ventasAnuales;
    }

    public void setVentasAnuales(BigDecimal ventasAnuales) {
        this.ventasAnuales = ventasAnuales;
    }

}
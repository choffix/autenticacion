/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
//import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
import javax.persistence.Table;
/**
 *
 * @author desa1
 */


@Entity
@Table(name="DEPARTAMENTOGEOGRAFICO")
public class DepartamentoGeografico extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_DPTO", length=3, nullable=false)
    private String codDpto;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;


    public DepartamentoGeografico() {
    }

    public DepartamentoGeografico(String cod) {
        this.codDpto = cod;
    }

    public String getCodDpto() {
        return codDpto;
    }

    public void setCodDpto(String codDpto) {
        this.codDpto = codDpto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codDpto != null ? codDpto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        DepartamentoGeografico other = (DepartamentoGeografico) object;
        if ((this.codDpto == null && other.codDpto != null) ||
        (this.codDpto != null && !this.codDpto.equals(other.codDpto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return   codDpto;
    }

}

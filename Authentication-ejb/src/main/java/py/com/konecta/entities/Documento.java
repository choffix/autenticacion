/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Kiki
 */
@Entity
@Table(name="DOCUMENTO")
public class Documento extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_DOCUMENTO", length=10, nullable=false)
    private String codDocumento;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;


    public Documento() {
    }

    public Documento(String codDocumento) {
        this.codDocumento = codDocumento;
    }

    public String getCodDocumento() {
        return codDocumento;
    }

    public void setCodDocumento(String codDocumento) {
        this.codDocumento = codDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codDocumento != null ? codDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.codDocumento == null && other.codDocumento != null) ||
        (this.codDocumento != null && !this.codDocumento.equals(other.codDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codDocumento + "]";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author desa2
 */
@Entity
@Table(name="ESTADO_FONDO")
public class EstadoFondo extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_EST_FON", length=10, nullable=false)
    private String codEstFon;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    public EstadoFondo() {
    }

    public EstadoFondo(String id) {
        this.codEstFon = id;
    }

    public EstadoFondo(EstadoFondoEnum estadoFondoEnum) {
        this.codEstFon = estadoFondoEnum.getCodEstadoFondo();
        this.nombre = estadoFondoEnum.getNombre();
        //this.codPadre = permisoEnum.getCodPadre();
    }

    public String getCodEstFon() {
        return codEstFon;
    }

    public void setCodEstFon(String codEstFon) {
        this.codEstFon = codEstFon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEstFon != null ? codEstFon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoFondo)) {
            return false;
        }
        EstadoFondo other = (EstadoFondo) object;
        if ((this.codEstFon == null && other.codEstFon != null) || (this.codEstFon != null && !this.codEstFon.equals(other.codEstFon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codEstFon + "]";
    }

    public static enum EstadoFondoEnum {

        PEN("PEN","PENDIENTE"),
        ACT("ACT","ACTIVO"),
        INA("INA","INACTIVO"),
        CER("CER","CERRADO");

        private final String codEstadoFondo;
        private final String nombre;

        private EstadoFondoEnum(String codEstadoFondo, String nombre) {
            this.codEstadoFondo = codEstadoFondo;
            this.nombre = nombre;
        }

        public String getCodEstadoFondo() {
            return codEstadoFondo;
        }

        public String getNombre() {
            return nombre;
        }
        

    }

}

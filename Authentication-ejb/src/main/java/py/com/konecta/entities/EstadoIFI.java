/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

/**
 *
 * @author desa1
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="ESTADOIFI")
public class EstadoIFI extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @Column(name="COD_ESTADOIFI", length=3, nullable=false)
    private String codigoEstado;
    
    @Column(name="NOMBRE_ESTADOIFI", length=50, nullable=false, unique=true)
    private String nombreEstado;

     public EstadoIFI(){
     }
     
     public EstadoIFI(String cod) {
        this.codigoEstado = cod;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

  @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoEstado != null ? codigoEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof ClasificacionBeneficiario)) {
            return false;
        }
        EstadoIFI other = (EstadoIFI) object;
        if ((this.codigoEstado == null && other.codigoEstado != null) ||
        (this.codigoEstado != null && !this.codigoEstado.equals(other.codigoEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombreEstado + "[" + codigoEstado + "]";
    }



}

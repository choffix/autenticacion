/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;


import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author desa3
 */
@Entity
@Table(name="ESTADO_USUARIO")
public class EstadoUsuario extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_EST_USU", length=10, nullable=false)
    private String codEstUsu;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    public EstadoUsuario() {
    }

    public EstadoUsuario(String id) {
        this.codEstUsu = id;
    }

    public String getCodEstUsu() {
        return codEstUsu;
    }

    public void setCodEstUsu(String codEstUsu) {
        this.codEstUsu = codEstUsu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEstUsu != null ? codEstUsu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoUsuario)) {
            return false;
        }
        EstadoUsuario other = (EstadoUsuario) object;
        if ((this.codEstUsu == null && other.codEstUsu != null) || (this.codEstUsu != null && !this.codEstUsu.equals(other.codEstUsu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codEstUsu + "]";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author Kiki
 */
@Entity
@Table(name="FONDO")
public class Fondo extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_FONDO", length=10, nullable=false)
     @GeneratedValue(generator = "fondoSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "fondoSeq", table = "SECUENCIA", pkColumnName = "NOMBRE_SEC",
    valueColumnName = "VALOR", pkColumnValue = "FONDO_SEQ", allocationSize = 1)
    private Integer codFondo;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    @Column(name="FECHA", nullable=false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha;

    @Column(name="MONTO",nullable=false, precision=17, scale=2)
    private BigDecimal monto;

    @Column(name="SALDO",nullable=false, precision=21, scale=2)
    private BigDecimal saldo;

    @Column(name="FACTOR_APALANCAMIENTO", length=10, nullable=false)
    private Integer factorApalancamiento;

    @Column(name="TASA_COBERTURA_MIN", precision=17, scale=2, nullable=false)
    private BigDecimal tasaCoberturaMin;

    @Column(name="TASA_COBERTURA_MAX", precision=17, scale=2, nullable=false)
    private BigDecimal tasaCoberturaMax;

    @Column(name="TASA_COMISION_MIN", precision=17, scale=2, nullable=false)
    private BigDecimal tasaComisionMin;

    @Column(name="TASA_COMISION_MAX", precision=17, scale=2, nullable=false)
    private BigDecimal tasaComisionMax;

    @Column(name="PRESTAMOS_CUBIERTOS_MAX",nullable=false, precision=17, scale=2)
    private BigDecimal prestamoCubiertoMax;

    @Column(name="PRESTAMOS_CUBIERTOS_MIN",nullable=false, precision=17, scale=2)
    private BigDecimal prestamoCubiertoMin;

    @Column(name="GARANTIA_MIN",nullable=false, precision=17, scale=2)
    private BigDecimal garantiaMin;

    @Column(name="GARANTIA_MAX",nullable=false, precision=17, scale=2)
    private BigDecimal garantiaMax;

    @Column(name="MONTO_PREVISION",nullable=true, precision=17, scale=2)
    private BigDecimal montoPrevision;

    @Column(name="MONTO_PAGADO",nullable=true, precision=17, scale=2)
    private BigDecimal montoPagado;

    //@ManyToOne
    //@JoinColumn(name = "COD_SECTOR", referencedColumnName = "COD_SECTOR")
    @ManyToMany
    @JoinTable(name="SECTOR_FONDO",
        joinColumns=@JoinColumn(name="COD_FONDO", referencedColumnName="COD_FONDO"),
        inverseJoinColumns=@JoinColumn(name="COD_SECTOR", referencedColumnName="COD_SECTOR"))
    private Collection<Sector> sectores;

    @ManyToOne
    @JoinColumn(name = "ID_MON", referencedColumnName = "ID_MON",nullable=false)
    private Moneda moneda;

    @ManyToOne
    @JoinColumn(name = "COD_EST_FON", referencedColumnName = "COD_EST_FON",nullable=false)
    private EstadoFondo estado;

    public Fondo() {
    }

    public Fondo(Integer codFondo) {
        this.codFondo = codFondo;
    }

    public Integer getCodFondo() {
        return codFondo;
    }

    public void setCodFondo(Integer codFondo) {
        this.codFondo = codFondo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Collection<Sector> getSectores() {
        return sectores;
    }

    public void setSectores(Collection<Sector> sectores) {
        this.sectores = sectores;
    }

    public EstadoFondo getEstado() {
        return estado;
    }

    public void setEstado(EstadoFondo estado) {
        this.estado = estado;
    }

    public Integer getFactorApalancamiento() {
        return factorApalancamiento;
    }

    public void setFactorApalancamiento(Integer factorApalancamiento) {
        this.factorApalancamiento = factorApalancamiento;
    }

    public BigDecimal getGarantiaMax() {
        return garantiaMax;
    }

    public void setGarantiaMax(BigDecimal garantiaMax) {
        this.garantiaMax = garantiaMax;
    }

    public BigDecimal getGarantiaMin() {
        return garantiaMin;
    }

    public void setGarantiaMin(BigDecimal garantiaMin) {
        this.garantiaMin = garantiaMin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrestamoCubiertoMax() {
        return prestamoCubiertoMax;
    }

    public void setPrestamoCubiertoMax(BigDecimal prestamoCubiertoMax) {
        this.prestamoCubiertoMax = prestamoCubiertoMax;
    }

    public BigDecimal getPrestamoCubiertoMin() {
        return prestamoCubiertoMin;
    }

    public void setPrestamoCubiertoMin(BigDecimal prestamoCubiertoMin) {
        this.prestamoCubiertoMin = prestamoCubiertoMin;
    }

    public BigDecimal getTasaCoberturaMax() {
        return tasaCoberturaMax;
    }

    public void setTasaCoberturaMax(BigDecimal tasaCoberturaMax) {
        this.tasaCoberturaMax = tasaCoberturaMax;
    }

    public BigDecimal getTasaCoberturaMin() {
        return tasaCoberturaMin;
    }

    public void setTasaCoberturaMin(BigDecimal tasaCoberturaMin) {
        this.tasaCoberturaMin = tasaCoberturaMin;
    }

    public BigDecimal getTasaComisionMax() {
        return tasaComisionMax;
    }

    public void setTasaComisionMax(BigDecimal tasaComisionMax) {
        this.tasaComisionMax = tasaComisionMax;
    }

    public BigDecimal getTasaComisionMin() {
        return tasaComisionMin;
    }

    public void setTasaComisionMin(BigDecimal tasaComisionMin) {
        this.tasaComisionMin = tasaComisionMin;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoPagado() {
        return montoPagado;
    }

    public void setMontoPagado(BigDecimal montoPagado) {
        this.montoPagado = montoPagado;
    }

    public BigDecimal getMontoPrevision() {
        return montoPrevision;
    }

    public void setMontoPrevision(BigDecimal montoPrevision) {
        this.montoPrevision = montoPrevision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFondo != null ? codFondo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Fondo)) {
            return false;
        }
        Fondo other = (Fondo) object;
        if ((this.codFondo == null && other.codFondo != null) ||
        (this.codFondo != null && !this.codFondo.equals(other.codFondo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codFondo + "]";
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="IFI")
public class IFI extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="IFISeq", strategy=GenerationType.TABLE)
    @TableGenerator(name="IFISeq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
    valueColumnName="VALOR", pkColumnValue="IFI_SEQ", allocationSize=1)
    @Column(name="IDIFI", precision=10, scale=0)
    private Integer idIfi;

    @Column(name="NOMBRE", length=100 , nullable=false)
    private String nombre;

    @Column(name="RAZON_SOCIAL", length=500 , nullable=false)
    private String razonSocial;

    @Column(name="RUC", length=30, nullable=false, unique=true)
    private String ruc;

    
    @Column(name="DIRECCION", length=100, nullable=false)
    private String direccion;

    @ManyToOne
    @JoinColumn(name = "COD_CIUDAD", referencedColumnName = "COD_CIUDAD")
    private Ciudad ciudad;
    
    @Column(name="TELEFONO", length=30, nullable=false)
    private String telefono;

    @Column(name="EMAIL", length=100, nullable=false)
    private String email;

    @Column(name="NRO_CONTRATO", length=30, nullable=false)
    private String nro_contrato;

    @Column(name="CAPITAL", nullable=false, precision=17, scale=2)
    private BigDecimal capital;

    @Column(name="LIMITE", nullable=false,  precision=17, scale=2)
    private BigDecimal limite;

    @Column(name="NROIFI", length=20, nullable=false, unique=true)
    private String nroIFI;

    @ManyToOne
    @JoinColumn(name = "COD_ESTADOIFI", referencedColumnName = "COD_ESTADOIFI", nullable=false)
    private EstadoIFI estadoIfi;

    @ManyToOne
    @JoinColumn(name = "COD_TIPO_IFI", referencedColumnName = "COD_TIPO_IFI")
    private TipoIFI tipoIF;

    @ManyToMany
    @JoinTable(name="IFIxFONDO",
        joinColumns=@JoinColumn(name="IDIFI", referencedColumnName="IDIFI"),
        inverseJoinColumns=@JoinColumn(name="COD_FONDO", referencedColumnName="COD_FONDO"))
    private Collection<Fondo> fondos;

   
    public IFI() {
    }

    public IFI(Integer id) {
        this.idIfi= id;
    }

    public BigDecimal getCapital() {
        return capital;
    }

    public void setCapital(BigDecimal capital) {
        this.capital = capital;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    
    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Collection<Fondo> getFondos() {
        return fondos;
    }

    public void setFondos(Collection<Fondo> fondos) {
        this.fondos = fondos;
    }

    public TipoIFI getTipoIF() {
        return tipoIF;
    }

    public void setTipoIF(TipoIFI tipoIF) {
        this.tipoIF = tipoIF;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
   
  
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public EstadoIFI getEstadoIfi() {
        return estadoIfi;
    }

    public void setEstadoIfi(EstadoIFI estadoIfi) {
        this.estadoIfi = estadoIfi;
    }

    public Integer getIdIfi() {
        return idIfi;
    }

    public void setIdIfi(Integer idIfi) {
        this.idIfi = idIfi;
    }

    public BigDecimal getLimite() {
        return limite;
    }

    public void setLimite(BigDecimal limite) {
        this.limite = limite;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(String nro_contrato) {
        this.nro_contrato = nro_contrato;
    }

    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNroIFI() {
        return nroIFI;
    }

    public void setNroIFI(String nroIFI) {
        this.nroIFI = nroIFI;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIfi != null ? idIfi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof IFI)) {
            return false;
        }
        IFI other = (IFI) object;
        if ((this.idIfi == null && other.idIfi != null) ||
        (this.idIfi != null && !this.idIfi.equals(other.idIfi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  nombre + "[" + idIfi + "]";
    }

}

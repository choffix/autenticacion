/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
/**
 *
 * @author desa3
 */
@Entity
@Table(name="MONEDA")
public class Moneda extends EntidadPadre implements Serializable {
     private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_MON", nullable = false)
    @GeneratedValue(generator = "monedaSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "monedaSeq", table = "SECUENCIA", pkColumnName = "NOMBRE_SEC",
    valueColumnName = "VALOR", pkColumnValue = "MONEDA_SEQ", allocationSize = 1)
    private Integer idMon;

    @Column(name="COD_MON", length=7, nullable=false)
    private String codMon;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    @Column(name="SIMBOL", length=4, nullable=false)
    private String simbolo;

    public Moneda() {
    }

    public Moneda(Integer codMon) {
        this.idMon = codMon;
    }

    public String getCodMon() {
        return codMon;
    }
  public Integer getIdMon() {
        return idMon;
    }

    public void setIdMon(Integer idMon) {
        this.idMon = idMon;
    }
    public void setCodMond(String codMon) {
        this.codMon = codMon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMon != null ? codMon.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Moneda)) {
            return false;
        }
        Moneda other = (Moneda) object;
        if ((this.codMon == null && other.codMon != null) ||
        (this.codMon != null && !this.codMon.equals(other.codMon))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codMon + "]";
    }

}

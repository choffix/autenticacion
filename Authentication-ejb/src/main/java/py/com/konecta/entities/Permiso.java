/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Kiki
 */
@Entity
@Table(name="PERMISO")
public class Permiso extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_PERMISO", length=10, nullable=false)
    private String codPermiso;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    @Column(name="DELEGABLE",nullable=false)
    private boolean delegable;
   
//    @Column(name="COD_PADRE", length=10, nullable=false)
//    private String codPadre;

    public Permiso() {
    }

    public Permiso(PermisoEnum permisoEnum) {
        this.codPermiso = permisoEnum.getCodPermiso();
        this.nombre = permisoEnum.getNombre();
        this.delegable = permisoEnum.getDelegable();
        //this.codPadre = permisoEnum.getCodPadre();
    }


    public Permiso(String codPermiso) {
        this.codPermiso = codPermiso;
    }

    public String getCodPermiso() {
        return codPermiso;
    }

    public void setCodPermiso(String codPermiso) {
        this.codPermiso = codPermiso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
      public void setDelegable(String delegable) {
        boolean b = Boolean.parseBoolean(delegable);
        this.delegable = b;
    }

    public boolean getDelegable() {
        return delegable;
    }
    
//    public String getCodPadre() {
//        return codPadre;
//    }
//
//    public void setCodPadre(String codPadre) {
//        this.codPadre = codPadre;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPermiso != null ? codPermiso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Permiso)) {
            return false;
        }
        Permiso other = (Permiso) object;
        if ((this.codPermiso == null && other.codPermiso != null) ||
        (this.codPermiso != null && !this.codPermiso.equals(other.codPermiso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codPermiso + "]";
    }

    public static enum PermisoEnum {


        P0("P0", "Ver Link Mantenimiento",Boolean.FALSE),
        P1("P1", "Ver Link IFI",Boolean.FALSE),
        P2("P2", "Ver Link Garantía",Boolean.FALSE),
        P3("P3", "Ver Link Cartera Credito",Boolean.FALSE),
        P4("P4", "Ver Link Reporte",Boolean.FALSE),
        P5("P5", "Ver Link Licitacion",Boolean.FALSE),
        P6("P6", "Ver Link Comision",Boolean.FALSE),

        //Acciones intra modulos

            //Mantenimiento
        P001("P001","Ver Link Rol",Boolean.FALSE),
        P002("P002","Ver Botones ABM Rol",Boolean.FALSE),
        P003("P003","Ver Link Fondo",Boolean.FALSE),
        P004("P004","Ver Botones ABM Fondo",Boolean.FALSE),
        P005("P005","Ver Link Sector",Boolean.FALSE),
        P006("P006","Ver Botones Sector",Boolean.FALSE),
        P007("P007","Ver Link Moneda",Boolean.FALSE),
        P008("P008","Ver Botones ABM Moneda",Boolean.FALSE),
        P009("P009","Ver Link Cotizaciones",Boolean.FALSE),
        P010("P010","Ver Botones ABM Cotizaciones",Boolean.FALSE),
        P011("P011","Ver Link Usuario",Boolean.FALSE),
        P012("P012","Ver Botones ABM Usuario",Boolean.FALSE),
        P013("P013","Ver Link Tipo Beneficiario",Boolean.FALSE),
        P014("P014","Ver Botones ABM Tipo Beneficiario",Boolean.FALSE),
        P015("P015","Ver Link Clasificacion Beneficiario",Boolean.FALSE),
        P016("P016","Ver Botones ABM Clasificacion Beneficiario",Boolean.FALSE),
        P017("P017","Ver Link Tipo IFI",Boolean.FALSE),
        P018("P018","Ver Botones ABM Tipo IFI",Boolean.FALSE),
        P019("P019","Ver Link Tipo Garantia Beneficiario",Boolean.FALSE),
        P020("P020","Ver Botones ABM Tipo Garantia Beneficiario",Boolean.FALSE),
        P021("P021","Ver Link Tipo Financiamiento",Boolean.FALSE),
        P022("P022","Ver Botones ABM Tipo Financiamiento",Boolean.FALSE),
        P023("P023","Ver Link Beneficiario",Boolean.FALSE),
        P024("P024","Ver Botones ABM Beneficiario",Boolean.FALSE),
        P025("P025","Ver Link Registro Sucesos",Boolean.FALSE),
        P034("P034","Ver Link Permiso",Boolean.FALSE),
        //IFI
        P101("P101","Ver Link IFI",Boolean.FALSE),
        P102("P102","Ver Botones ABM IFI",Boolean.FALSE),
        P103("P103","Ver Link Sucursal",Boolean.FALSE),
        P104("P104","Ver Botones ABM Sucursal",Boolean.FALSE),
        P105("P105","Ver Link Firmantes de Contrato",Boolean.FALSE),
        P106("P106","Ver Botones ABM Firmantes de Contrato",Boolean.FALSE),

        //Garantia

         P201("P201","Ver Link Reserva Garantia",Boolean.FALSE),
         P202("P202","Ver Botones ABM Reserva Garantia",Boolean.FALSE),
         P203("P203","Ver Link Aprobar Reserva Garantia",Boolean.FALSE),
         P204("P204","Ver Boton Aprobar Reserva Garantia",Boolean.FALSE),
         P205("P205","Ver Link Desembolso Prestamo",Boolean.FALSE),
         P206("P206","Ver Boton Desembolso Prestamo",Boolean.FALSE),
         P207("P207","Ver Link Garantia Adicional",Boolean.FALSE),
         P208("P208","Ver Boton Garantia Adicional",Boolean.FALSE),


         //Cartera Credito
         P301("P301","Ver Link Cartera Credito",Boolean.FALSE),
         P302("P302","Ver Botones ABM Cartera Credito",Boolean.FALSE),
         P303("P303","Ver Boton Subir Archivo",Boolean.FALSE),

         //Reporte
         P401("P401","Ver los Link de los Reportes",Boolean.FALSE),

         //Licitacion
         P501("P501","Ver Link Licitacion",Boolean.FALSE),
         P502("P502","Ver Boton ABM Licitacion",Boolean.FALSE),
         P503("P503","Ver Boton Activar Licitacion",Boolean.FALSE),
         P504("P504","Ver Link Oferta Licitacion",Boolean.FALSE),
         P505("P505","Ver Boton ABM Oferta Licitacion",Boolean.FALSE),
         P506("P506","Ver Link Asignacion Cupo Licitacion",Boolean.FALSE),
         P507("P507","Ver Boton ABM Asignacion Cupo Licitacion",Boolean.FALSE),

         //Comision
         P601("P601","Ver Link Comision",Boolean.FALSE),
         P602("P602","Ver Boton ABM Comision",Boolean.FALSE);


        private final String codPermiso;
        private final String nombre;
        private final boolean delegable;

        private PermisoEnum(String codPermiso, String nombre, boolean delegable) {
            this.codPermiso = codPermiso;
            this.nombre = nombre;
            this.delegable = delegable;
        }

        public String getCodPermiso() {
            return codPermiso;
        }

        public String getNombre() {
            return nombre;
        }

        public boolean getDelegable() {
            return delegable;
        }

        
    }

}
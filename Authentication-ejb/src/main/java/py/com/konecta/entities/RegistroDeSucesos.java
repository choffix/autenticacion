/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Kiki
 */
@Entity
@Table(name="REGISTRO_SUCESOS")
public class RegistroDeSucesos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="regsucSeq", strategy=GenerationType.TABLE)
    @TableGenerator(name="regsucSeq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
    valueColumnName="VALOR", pkColumnValue="REGISTRO_SUCESOS_SEQ", allocationSize=1)
    @Column(name="ID_SUCESO",precision=10, scale=0)
    private Integer idSuceso;

    @Column(name="COD_REGISTRO_SUCESOS", length=10, nullable=false)
    private String codRegSuc;

    @Column(name="USUARIO", length=50, nullable=false)
    private String usuario;

    @Column(name="IFI", length=50, nullable=false)
    private String ifi;

    @Column(name="FECHA", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name="DESCRIPCION", length=500, nullable=false)
    private String descripcion;


    public RegistroDeSucesos() {
    }

    public RegistroDeSucesos(String codRegSuc) {
        this.codRegSuc = codRegSuc;
    }

    public String getCodRegSuc() {
        return codRegSuc;
    }

    public void setCodRegSuc(String codRol) {
        this.codRegSuc = codRol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIdSuceso() {
        return idSuceso;
    }

    public void setIdSuceso(Integer idsuceso) {
        this.idSuceso = idsuceso;
    }

    public String getIfi() {
        return ifi;
    }

    public void setIfi(String ifi) {
        this.ifi = ifi;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRegSuc != null ? codRegSuc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        RegistroDeSucesos other = (RegistroDeSucesos) object;
        if ((this.codRegSuc == null && other.codRegSuc != null) ||
        (this.codRegSuc != null && !this.codRegSuc.equals(other.codRegSuc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return usuario + "[" + codRegSuc + "]";
    }

}

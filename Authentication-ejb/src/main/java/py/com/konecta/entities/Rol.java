/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author Kiki
 */
@Entity
@Table(name="ROL")
public class Rol extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_ROL", length=15, nullable=false)
    @GeneratedValue(generator="rolSeq", strategy=GenerationType.TABLE)
     @TableGenerator(name="rolSeq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
     valueColumnName="VALOR", pkColumnValue="ROL_SEQ", allocationSize=1)
    private Integer codRol;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;
    
    @Column(name="DELEGABLE", nullable=false)
    private boolean delegable;

    @ManyToMany
    @JoinTable(name="PERMISO_ROL", 
        joinColumns=@JoinColumn(name="COD_ROL", referencedColumnName="COD_ROL"),
        inverseJoinColumns=@JoinColumn(name="COD_PERMISO", referencedColumnName="COD_PERMISO"))
    private Collection<Permiso> permisos;

    public Rol() {
    }

    public Rol(Integer codRol) {
        this.codRol = codRol;
    }

    public Integer getCodRol() {
        return codRol;
    }

    public void setCodRol(Integer codRol) {
        this.codRol = codRol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isDelegable() {
        return delegable;
    }
    
    public void setDelegable(String delegable) {
        boolean b = Boolean.parseBoolean(delegable);
        this.delegable = b;
    }

    public Collection<Permiso> getPermisos() {
        return permisos;
    }

    public void setPermisos(Collection<Permiso> permisos) {
        this.permisos = permisos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRol != null ? codRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        Rol other = (Rol) object;
        if ((this.codRol == null && other.codRol != null) ||
        (this.codRol != null && !this.codRol.equals(other.codRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codRol + "]";
    }

}

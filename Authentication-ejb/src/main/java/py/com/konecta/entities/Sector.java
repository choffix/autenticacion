/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author desa2
 */
@Entity
@Table(name="SECTOR")
public class Sector extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_SECTOR", length=10, nullable=false)
    @GeneratedValue(generator="sectorSeq", strategy=GenerationType.TABLE)
    @TableGenerator(name="sectorSeq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
    valueColumnName="VALOR", pkColumnValue="SECTOR_SEQ", allocationSize=1)
    private Integer codSector;

    @Column(name="NOMBRE", length=100, nullable=false)
    private String nombre;

    @Column(name="DESCRIPCION", length=500, nullable=false)
    private String descripcion;


    public Sector() {
    }

    public Sector(Integer codSector) {
        this.codSector = codSector;
    }

    public Integer getCodSector() {
        return codSector;
    }

    public void setCodSector(Integer codSector) {
        this.codSector = codSector;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSector != null ? codSector.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Sector)) {
            return false;
        }
        Sector other = (Sector) object;
        if ((this.codSector == null && other.codSector != null) ||
        (this.codSector != null && !this.codSector.equals(other.codSector))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codSector + "]";
    }

}


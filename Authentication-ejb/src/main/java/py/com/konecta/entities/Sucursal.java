 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="SUCURSAL")
public class Sucursal  extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="sucursal_seq", strategy=GenerationType.TABLE)
    @TableGenerator(name="sucursal_seq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
    valueColumnName="VALOR", pkColumnValue="SUCURSAL_SEQ", allocationSize=1)
    @Column(name="ID_SUCURSAL", precision=10, scale=0)
    private Long iD_sucursal;

    @Column(name="COD_SUCURSAL", length=30 , nullable=false, unique=true)
    private String codSucursal;

    @Column(name="DIRECCION", length=100, nullable=false)
    private String direccion;

    @ManyToOne
    @JoinColumn(name = "COD_CIUDAD", referencedColumnName = "COD_CIUDAD")
    private Ciudad ciudad;

    @ManyToOne
    @JoinColumn(name = "COD_DPTO", referencedColumnName = "COD_DPTO")
    private DepartamentoGeografico departamento;

   @Column(name="TELEFONO", length=50, nullable=false)
    private String telefono;

   @Column(name="NOMBRE", length=50, nullable=false)
   private String nombre;
   
    @ManyToOne
    @JoinColumn(name = "IDIFI", referencedColumnName = "IDIFI")
    private IFI ifi;

    public Sucursal() {
    }

    public Sucursal(Long idSUcursal) {
        this.iD_sucursal = idSUcursal;
    }

    public String getCodSucursal() {
        return codSucursal;
    }

    public void setCodSucursal(String codSucursal) {
        this.codSucursal = codSucursal;
    }

    public DepartamentoGeografico getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoGeografico departamento) {
        this.departamento = departamento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getiD_sucursal() {
        return iD_sucursal;
    }

    public void setiD_sucursal(Long iD_sucursal) {
        this.iD_sucursal = iD_sucursal;
    }

    public IFI getIfi() {
        return ifi;
    }

    public void setIfi(IFI ifi) {
        this.ifi = ifi;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }
    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iD_sucursal != null ? iD_sucursal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Sucursal other = (Sucursal) object;
        if ((this.iD_sucursal == null && other.iD_sucursal != null) ||
        (this.iD_sucursal != null && !this.iD_sucursal.equals(other.iD_sucursal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codSucursal +  "[" + iD_sucursal + "]";
    }

}

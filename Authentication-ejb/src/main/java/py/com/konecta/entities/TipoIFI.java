/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author desa3
 */
@Entity
@Table(name="TIPO_IFI")
public class TipoIFI extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="COD_TIPO_IFI", length=5, nullable=false)
    @GeneratedValue(generator = "tipoIfiSeq", strategy = GenerationType.TABLE)
    @TableGenerator(name = "tipoIfiSeq", table = "SECUENCIA", pkColumnName = "NOMBRE_SEC",
    valueColumnName = "VALOR", pkColumnValue = "TIPOIFI_SEQ", allocationSize = 1)
    private Integer codTipoIFI;

    @Column(name="NOMBRE", length=50, nullable=false)
    private String nombre;

    @Column(name="MAXP_TIPO_IFI",  precision=17, scale=2, nullable=false)
    private BigDecimal maxpTipoIFI;

    public TipoIFI() {
    }

    public TipoIFI(Integer id) {
        this.codTipoIFI= id;
    }

    public Integer getCodTipoIfi() {
        return codTipoIFI;
    }

    public void setCodTipoIfi(Integer codTipoIfi) {
        this.codTipoIFI = codTipoIfi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getMaxpTipoIFI() {
        return maxpTipoIFI;
    }

    public void setMaxpTipoIFI(BigDecimal maxpTipoIFI) {
        this.maxpTipoIFI = maxpTipoIFI;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoIFI != null ? codTipoIFI.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoIFI)) {
            return false;
        }
        TipoIFI other = (TipoIFI) object;
        if ((this.codTipoIFI == null && other.codTipoIFI != null) || (this.codTipoIFI != null && !this.codTipoIFI.equals(other.codTipoIFI))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre + "[" + codTipoIFI + "]";
    }

}

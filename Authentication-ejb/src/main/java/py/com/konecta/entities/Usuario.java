/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author Lamia
 */
@Entity
@Table(name="USUARIO")
public class Usuario extends EntidadPadre implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator="usuarioSeq", strategy=GenerationType.TABLE)
    @TableGenerator(name="usuarioSeq", table="SECUENCIA", pkColumnName="NOMBRE_SEC",
    valueColumnName="VALOR", pkColumnValue="USUARIO_SEQ", allocationSize=1)
    @Column(name="ID_USUARIO",  precision=10, scale=0)
    private Integer idUsuario;

    @Column(name="NOMBRE_USUARIO", length=50 , nullable=false, unique=true)
    private String nombreUsuario;

    @Column(name="CONTRASENHA", length=50, nullable=false)
    private String contrasenha;

    @Column(name="NOMBRES", length=50, nullable=false)
    private String nombres;

    @Column(name="APELLIDOS", length=50, nullable=false)
    private String apellidos;

    @Column(name="NRO_DOCUMENTO", length=30, nullable=false)
    private String nroDocumento;

    @Column(name="TELEFONO", length=50, nullable=false)
    private String telefono;

    @Column(name="EMAIL", length=50, nullable=false)
    private String email;
    
    @Column(name="ADMINIFI", nullable=false)
    private Boolean adminifi;
    
    @Column(name="CAMBIARPASSWORD", nullable=false)
    private Boolean cambiarpassword;

    @Column(name="FECHA_ULTIMO_INGRESO", nullable=true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaUltimoIngreso;
    
    @Column(name="INTENTO_FALLIDO",nullable=true)
    private Integer intentoFallido;
    
    @Column(name="PASSWORDPERMANENTE", length=50, nullable=true)
    private String passwordPermanente;
    
    @Column(name="CREADO_POR", length=30, nullable=false)
    private String creadoPor;

    @Column(name="MODIFICADO_POR", length=30, nullable=false)
    private String modificadoPor;
    
    @Column(name="FECHA_MODIFICACION", nullable=true)
    private Date fechaModificacion;

    @ManyToOne
    @JoinColumn(name = "COD_EST_USU", referencedColumnName = "COD_EST_USU")
    private EstadoUsuario estado;

    @ManyToOne
    @JoinColumn(name = "COD_DOCUMENTO", referencedColumnName = "COD_DOCUMENTO")
    private Documento tipoDocumento;

    @ManyToOne
    @JoinColumn(name = "IDIFI", referencedColumnName = "IDIFI", nullable=true)
    private IFI ifi;
    
     @ManyToOne
    @JoinColumn(name = "ID_SUCURSAL", referencedColumnName = "ID_SUCURSAL", nullable=true)
    private Sucursal sucursal;

    @ManyToMany
    @JoinTable(name="ROL_USUARIO",
        joinColumns=@JoinColumn(name="ID_USUARIO", referencedColumnName="ID_USUARIO"),
        inverseJoinColumns=@JoinColumn(name="COD_ROL", referencedColumnName="COD_ROL"))
    private Collection<Rol> roles;

    public Usuario() {
    }

    public Usuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getContrasenha() {
        return contrasenha;
    }

    public void setContrasenha(String contrasenha) {
        this.contrasenha = contrasenha;
    }

    public IFI getIfi() {
        return ifi;
    }

    public void setIfi(IFI ifi) {
        this.ifi = ifi;
    }
      public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
        public Boolean getAdminifi() {
        return adminifi;
    }

    public void setAdminifi(String adminifi) {
        this.adminifi = Boolean.parseBoolean(adminifi);
    }

    public Date getFechaUltimoIngreso() {
        return fechaUltimoIngreso;
    }

    public void setFechaUltimoIngreso(Date fechaUltimoIngreso) {
        this.fechaUltimoIngreso = fechaUltimoIngreso;
    }

    public String getCreadoPor() {
        return creadoPor;
    }

    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }
    
    

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    

    public EstadoUsuario getEstado() {
        return estado;
    }

    public void setEstado(EstadoUsuario estado) {
        this.estado = estado;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Collection<Rol> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Rol> roles) {
        this.roles = roles;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Documento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Documento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Boolean getCambiarpassword() {
        return cambiarpassword;
    }

    public void setCambiarpassword(String cambiarpassword) {
        this.cambiarpassword = Boolean.parseBoolean(cambiarpassword);
    }

    public Integer getIntentoFallido() {
        return intentoFallido;
    }

    public void setIntentoFallido(Integer intentoFallido) {
        this.intentoFallido = intentoFallido;
    }

    public String getPasswordPermanente() {
        return passwordPermanente;
    }

    public void setPasswordPermanente(String passwordPermanente) {
        this.passwordPermanente = passwordPermanente;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the codPermiso fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) ||
        (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return apellidos + ", " + nombres + "[" + idUsuario + "]";
    }

}

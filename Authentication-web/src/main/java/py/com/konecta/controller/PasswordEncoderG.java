package py.com.konecta.controller;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import py.com.konecta.ejb.UsuarioManagerLocal;
import py.com.konecta.entities.Usuario;


public class PasswordEncoderG extends Md5PasswordEncoder {

	private UsuarioManagerLocal usuarioManager;

	private Context context;

	/**
	 * @param encPass
	 *            : password encriptado.
	 * @param rawPass
	 *            : password en texto plano.
	 * @param salt
	 *            : username.
	 * @return boolean : true para valido, false para invalido.
	 */

	@Override
	public boolean isPasswordValid(String encPass, String rawPass, Object salt)
			throws BadCredentialsException {
		String pass2 = encodePassword(rawPass, salt);
		Usuario usuario = null;
		try {
			Usuario ejemplo = new Usuario();
			ejemplo.setNombreUsuario(salt.toString());
			ejemplo.setContrasenha(pass2);

			inicializarUsuarioManager();

			usuario = usuarioManager.get(ejemplo);

			if (usuario == null) {
				throw new BadCredentialsException("Usuario y pass no valido");
			}

			return true;

		} catch (Exception ex) {
			throw new BadCredentialsException(ex.getMessage());
		}

	}

	private void inicializarUsuarioManager() {
		if (context == null)
			try {
				context = new InitialContext();
			} catch (NamingException e1) {
				throw new RuntimeException(
						"No se puede inicializar el contexto", e1);
			}
		if (usuarioManager == null) {
			try {

				usuarioManager = (UsuarioManagerLocal) context
						.lookup("java:global/Authentication-ear-1.0/Authentication-ejb-1.0/UsuarioManager!py.com.konecta.ejb.UsuarioManagerLocal");
			} catch (NamingException ne) {
				throw new RuntimeException(
						"No se encuentra EJB valor Manager: ", ne);
			}
		}
	}

}

package py.com.konecta.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.Context;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import py.com.konecta.ejb.UsuarioManagerLocal;
import py.com.konecta.entities.Usuario;

/**
 * @author choffix
 * 
 */
@Service("UserDetailServiceG")
public class UserDetailServiceG implements UserDetailsService {

	Context context;

	@EJB(mappedName = "java:global/Authentication-ear-1.0/Authentication-ejb-1.0/UsuarioManager!py.com.konecta.ejb.UsuarioManagerLocal")
	private UsuarioManagerLocal usuarioManager;


	/**
	 * Returns a populated {@link UserDetailsG} object. The username is first
	 * retrieved from the database and then mapped to a {@link UserDetailsG}
	 * object.
	 * 
	 * @param username
	 * @return
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		try {

			Usuario ejemplo = new Usuario();
			ejemplo.setNombreUsuario(username);

			Usuario datosUsuario = usuarioManager.get(ejemplo);

			if (datosUsuario == null) {
				throw new Exception("No se ha encontrado el usuario " + username);
			}

			String nombre = "";

			if (datosUsuario.getNombreUsuario() != null
					&& !datosUsuario.getNombreUsuario().isEmpty()) {
				nombre = datosUsuario.getNombreUsuario().toString();
			}
                        
                        List<Map<String, Object>> roles;
                    roles = usuarioManager.listarRoles(datosUsuario.getIdUsuario());

			List<GrantedAuthority> permisos = new ArrayList<GrantedAuthority>();
			SimpleGrantedAuthority p;
                        for(Map<String, Object> rol:roles){
                            p = new SimpleGrantedAuthority(rol.get("nombre").toString());
                            permisos.add(p);
                        }
                    
			return new UserDetailsG(username, datosUsuario.getContrasenha()
					.toString(), permisos, nombre, datosUsuario.getIdUsuario()) {
			};
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}

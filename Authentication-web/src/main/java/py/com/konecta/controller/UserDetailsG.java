package py.com.konecta.controller;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class UserDetailsG extends User {

	private static final long serialVersionUID = 1L;
	private Integer idUsuario;
	private String nombreCompleto;
	private String nombreRol;

	public UserDetailsG(String username, String password,
			Collection<? extends GrantedAuthority> authorities,
			String nombreCompleto, Integer pkUsuario) {

		super(username, password, authorities);

		this.idUsuario = pkUsuario;
		this.nombreCompleto = nombreCompleto;

	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getNombreRol() {
		return nombreRol;
	}

	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}

	public static UserDetailsG getUsuarioAutenticado() {
		return (UserDetailsG) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
	}

	public boolean tengoPermiso(String modulo, String permiso) {

		String permisoSolicitado = modulo + "." + permiso;
		if (getUsuarioAutenticado().getAuthorities() != null) {
			for (GrantedAuthority perm : getUsuarioAutenticado()
					.getAuthorities()) {
				if (perm.getAuthority().equals(permisoSolicitado)) {
					return true;
				}
			}
		}
		return false;
	}
}

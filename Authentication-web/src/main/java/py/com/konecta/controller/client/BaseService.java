/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.controller.client;

import com.google.gwt.user.client.rpc.RemoteService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import py.com.konecta.controller.client.gui.utils.BaseRemoteException;

/**
 *
 * @author alfonso
 */
public interface BaseService extends RemoteService{

    public String guardar(String jsonString) throws BaseRemoteException;
    
    public String guardar(String jsonString, Integer valor) throws BaseRemoteException;

    public String actualizar(String jsonString) throws BaseRemoteException;

    public String eliminar(String jsonString) throws BaseRemoteException;

    public List<String> listar(String ejemploJson, String [] atributos, boolean like,
            boolean all, Integer primerResultado, Integer cantResultados,
            String [] orderBy, String[] direccion)  throws BaseRemoteException;;

    public List<String> listar(String ejemploJson, String [] atributos,
            String orderBy, String direccion) throws BaseRemoteException;;

    public List<String> traerLista(String ejemploJson, Integer primerResultado, 
            Integer cantResultados, String[] atributos) throws BaseRemoteException;

    public Integer getTotalRows(String ejemploJson) throws BaseRemoteException;

    public BigDecimal dummyBigDecimal(BigDecimal dummyBigDecimal)
            throws BaseRemoteException;
   


}

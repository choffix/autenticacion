/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.controller.client.gui.utils;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author Kiki
 */
public class BaseRemoteException extends Exception implements IsSerializable {

    public BaseRemoteException(Throwable cause) {
        super(cause);
    }

    public BaseRemoteException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseRemoteException(String message) {
        super(message);
    }

    public BaseRemoteException() {
        super();
    }
}

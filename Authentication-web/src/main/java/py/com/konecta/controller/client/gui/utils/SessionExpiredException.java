/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.controller.client.gui.utils;

/**
 *
 * @author emeza2
 */
public class SessionExpiredException extends BaseRemoteException {

    public SessionExpiredException() {
    }

    public SessionExpiredException(String message) {
    }

    public SessionExpiredException(String message, Throwable cause) {
    }

    public SessionExpiredException(Throwable cause) {
    }
}

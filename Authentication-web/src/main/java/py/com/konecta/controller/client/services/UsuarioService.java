/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.konecta.controller.client.services;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;
import java.util.Map;
import py.com.konecta.controller.client.BaseService;
import py.com.konecta.controller.client.gui.utils.BaseRemoteException;
/**
 *
 * @author desa1
 */
@RemoteServiceRelativePath("UsuarioService")
public interface UsuarioService extends BaseService {

    public List<String> listarRoles(Integer codigo) throws BaseRemoteException;

    public List<String> listarRolesUser(Integer codigo) throws BaseRemoteException;

    public Map<String,Object> autenticar(String nombreUsuario, String contrasenha) throws BaseRemoteException;

    public String autenticarContrasenha(String userName, String contrasenha) throws BaseRemoteException;

    public List<String> listarPermisos(Integer idUsuario) throws BaseRemoteException;

    public Boolean controlarViolacionDeCodigo(String codigo)throws BaseRemoteException;

    public String actualizarEstadoUsuario(String idUsuario, String Estado);
    
    public String validarUsuarioPassword(String nombre, String password) throws BaseRemoteException;
    
    public Boolean controlarViolacionDeCodigo2(String codigo)throws BaseRemoteException;
    
    public Integer cantidadIntentos(String nombreUsuario);
    
    public String setearCantidadIntentos(String nombreUsuario,Integer cantidad);
    
    public String intentoFallidoUsuarioInactivo(Integer idUsuario, String estado);
    
    public String encriptarPasswordTemporal(String password);
    
    public String consultaPasswordPermanente(String userName,String pass);
    
    public List<Map<String, Object>> listarUsuariosPorInstitucion(Integer idUsuario) throws BaseRemoteException;
}


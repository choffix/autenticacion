/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.controller.service;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpSession;
import py.com.konecta.controller.client.BaseService;
import py.com.konecta.controller.client.gui.utils.SessionExpiredException;
/**
 *
 * @author Kiki
 */
public abstract class BaseServiceImpl extends RemoteServiceServlet implements BaseService {
   
    //@EJB
    //RolManagerLocal rolManager;
    public BigDecimal dummyBigDecimal(BigDecimal dumyBigDecimal) {
        throw new UnsupportedOperationException("Solo incluido para forzar la serializacion de tipos");
    }

    public abstract Integer getTotalRows(String ejemploJson);

    public abstract List<String> traerLista(String ejemploJson, Integer primerResultado, Integer cantResultados, String[] atributos);

    /**
     * metodos para el control de login y tiempo de session
     */

    public String getNombreUsuario() {
        HttpSession session = this.getThreadLocalRequest().getSession();
        String resultado = "";
        resultado = (String) session.getAttribute("nombreUsuario");
        resultado = (String) session.getAttribute("apellidoUsuario") + " - "+resultado;
        return resultado;
    }
  
     public String getEntidadFinanciera() {
        HttpSession session = this.getThreadLocalRequest().getSession();
        return (String) session.getAttribute("entidadFinanciera");
    }

    public List<String> getPermisos()
    {
        HttpSession session = this.getThreadLocalRequest().getSession();
        HashMap<String,Object> permisos = (HashMap<String,Object>)session.getAttribute("permisoXUsuario");
        List<String> listaPermiso = new ArrayList<String>();
        if(permisos != null)
        {
            Iterator<String> keys = permisos.keySet().iterator();
            while(keys.hasNext())
            {
                listaPermiso.add(keys.next());
            }
            return listaPermiso;
        }
        return listaPermiso;
    }

    public Integer getIdIfi()
    {
        HttpSession session = this.getThreadLocalRequest().getSession();
        Integer idIfi = (Integer)session.getAttribute("idIfi");
        if(idIfi != null)
            return idIfi;
        return -1;
    }

    @Override
    public String processCall(String payload) throws SerializationException {
        HttpSession session = getThreadLocalRequest().getSession(false);
//        String nombreUsuario, entidadFinanciera;
//        Long selloTiempo,selloTiempoActual;
        if (session != null) {
//            nombreUsuario = (String)session.getAttribute("nombreUsuario");
//            entidadFinanciera = (String)session.getAttribute("entidadFinaciera");
//            selloTiempo = (Long)session.getAttribute("timeStamp");
//            if(selloTiempo != null)
//            {
//                selloTiempoActual = Calendar.getInstance().getTimeInMillis() / 1000;
//                if(selloTiempoActual - selloTiempo <= session.getMaxInactiveInterval())
//                {
//                    session.setAttribute("timeStamp", selloTiempoActual);
//                }
//            }
            return super.processCall(payload);
        } else {
            //session.invalidate();
            return RPC.encodeResponseForFailure(null, new SessionExpiredException(""));
            //return RPC.encodeResponseForFailure(null, new SessionExpiredException(""));

        }


    }

   

}

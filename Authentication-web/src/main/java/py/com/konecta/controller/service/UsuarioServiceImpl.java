/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.controller.service;

import com.google.gwt.user.client.rpc.SerializationException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpSession;
import py.com.konecta.controller.client.gui.utils.Base64Utils;
import py.com.konecta.controller.client.gui.utils.BaseRemoteException;
import py.com.konecta.controller.client.services.UsuarioService;
import py.com.konecta.controller.service.utils.json.JSONArray;
import py.com.konecta.controller.service.utils.json.JSONException;
import py.com.konecta.controller.service.utils.json.JSONObject;
import py.com.konecta.ejb.UsuarioManagerLocal;
import py.com.konecta.ejb.utils.Log;
import py.com.konecta.ejb.utils.SHA1;
import py.com.konecta.entities.Documento;
import py.com.konecta.entities.EstadoUsuario;
import py.com.konecta.entities.IFI;
import py.com.konecta.entities.RegistroDeSucesos;
import py.com.konecta.entities.Rol;
import py.com.konecta.entities.Sucursal;
import py.com.konecta.entities.Usuario;


/**
 *
 * @author desa1
 */
public class UsuarioServiceImpl extends BaseServiceImpl implements UsuarioService {

    @EJB
    UsuarioManagerLocal usuarioManager;
    
    Log log = new Log();
    private String idUsuarioBase64;
    private String nombreUsuarioBase64;
    private String ifiBase64;
    private String nombresBase64;
    private String apellidosBase64;
    private String adminIfiBase64;
    private String cambiarPasswordBase64;
    private String nroDocumentoBase64;
    private String telefonoBase64;
    private String emailBase64;
    private String estadoBase64;
    private String tipoDocumentoBase64;
    private String sucursalBase64;
    private String contrasenhaNuevaBase64;
    private String shaBase64;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    private static final String IGUAL = "IGUAL";
    private static final String USUARIO = "USUARIO";
    private static final String PASSWORD = "PASSWORD";

    public UsuarioServiceImpl() {
    }

    public String guardar(String jsonString) {
        JSONObject resultadoJson = new JSONObject();
        String Contrasenha = "null";
        String nombreUsuario = "null";
        try {

            JSONObject usuarioJson = new JSONObject(jsonString);

            Usuario usuario = new Usuario();
            //usuario.setIdUsuario(usuarioJson.getString("codRol"));
            usuario.setNombreUsuario(usuarioJson.getString("nombreUsuario"));
            nombreUsuario = usuarioJson.getString("nombreUsuario");
            if (usuarioJson.has("contrasenha")) {
                usuario.setContrasenha(SHA1.getHash(usuarioJson.getString("contrasenha")));
            }
            else{
                Contrasenha = generarPasswordAleatorio();
                usuario.setContrasenha(SHA1.getHash(Contrasenha));
            }
            usuario.setEstado(new EstadoUsuario(usuarioJson.getString("estado")));

            if (usuarioJson.has("nombres")) {
                usuario.setNombres(usuarioJson.getString("nombres"));
            }
            if (usuarioJson.has("apellidos")) {
                usuario.setApellidos(usuarioJson.getString("apellidos"));
            }
            if (usuarioJson.has("tipoDocumento")) {
                usuario.setTipoDocumento(new Documento(usuarioJson.getString("tipoDocumento")));
            }
            if (usuarioJson.has("nroDocumento")) {
                usuario.setNroDocumento(usuarioJson.getString("nroDocumento"));
            }
            if (usuarioJson.has("telefono")) {
                usuario.setTelefono(usuarioJson.getString("telefono"));
            }
            if (usuarioJson.has("ifi")) {
                usuario.setIfi(new IFI(Integer.parseInt(usuarioJson.getString("ifi"))));
            }
             if (usuarioJson.has("sucursal")) {
                usuario.setSucursal(new Sucursal(Long.parseLong(usuarioJson.getString("sucursal"))));
            }
            if (usuarioJson.has("adminifi")) {
                 usuario.setAdminifi(usuarioJson.getString("adminifi"));
            }
            if (usuarioJson.has("cambiarpassword")) {
                 usuario.setCambiarpassword(usuarioJson.getString("cambiarpassword"));
            }

            if (usuarioJson.has("email")) {
                usuario.setEmail(usuarioJson.getString("email"));
            }
            if (usuarioJson.has("roles")) {
                JSONArray rolesJson = usuarioJson.getJSONArray("roles");
                ArrayList<Rol> rolesUsuario = new ArrayList<Rol>();
                for (int i = 0; i < rolesJson.length(); i++) {
                    rolesUsuario.add(new Rol(Integer.parseInt(rolesJson.getString(i))));
                }
                usuario.setRoles(rolesUsuario);
            }
            if(usuarioJson.has("creadoPor")){
                usuario.setCreadoPor(usuarioJson.getString("creadoPor"));
            }
            usuario.setFechaModificacion(Calendar.getInstance().getTime());
            
            usuario.setEntidadFinancieraSys(getEntidadFinanciera());
            usuario.setUsuarioSys(getNombreUsuario());
            usuarioManager.save(usuario);

        } catch (Exception e) {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrió un error al guardar la Entidad: Usuario");
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(getNombreUsuario());
            sucesos.setIfi(getEntidadFinanciera());
            usuarioManager.huellaAuditoria(sucesos);
            e.printStackTrace();
            try {
                resultadoJson.put("resultado", "Error al guardar el nuevo usuario");
            } catch (JSONException ex) {
                ex.printStackTrace();
           //     Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return resultadoJson.toString();
        }
        try {
            resultadoJson.put("resultado", "Usuario agregado correctamente");
            resultadoJson.put("contrasenha", Contrasenha);
            Usuario user = new Usuario();
            user.setNombreUsuario(nombreUsuario);
            Map<String, Object> result = usuarioManager.getAtributos(user,
                        new String[]{"idUsuario"});
            resultadoJson.put("idUsuario", result.get("idUsuario").toString());
        } catch (JSONException ex) {
            ex.printStackTrace();
//            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultadoJson.toString();
    }

    public String actualizar(String jsonString) {
        JSONObject resultadoJson = new JSONObject();
        String Contrasenha = "null";
        String idUsuario = "null";
        try {
            JSONObject usuarioJson = new JSONObject(jsonString);
            Usuario usuario = new Usuario();
            if (usuarioJson.has("cambioContrasenha")) {
                desencriptarDatos(usuarioJson);
                usuario.setIdUsuario(Integer.parseInt(idUsuarioBase64));
                Map<String, Object> result = usuarioManager.getAtributos(usuario,
                        new String[]{"idUsuario", "nombreUsuario", "nombres", "apellidos", "tipoDocumento.codDocumento", "adminifi", "cambiarpassword",
                            "ifi.idIfi", "sucursal.iD_sucursal", "nroDocumento", "telefono", "email", "fechaUltimoIngreso",
                            "estado.codEstUsu", "passwordPermanente", "contrasenha", "creadoPor"});

                usuario.setIdUsuario(Integer.parseInt(result.get("idUsuario").toString()));
                usuario.setNombreUsuario(result.get("nombreUsuario").toString());
                if (result.get("creadoPor") != null) {
                    usuario.setCreadoPor(result.get("creadoPor").toString());
                }

                if (usuarioJson.has("cambiarpassword")) {
                    usuario.setCambiarpassword(usuarioJson.getString("cambiarpassword"));
                    usuario.setPasswordPermanente(result.get("contrasenha").toString());
                }
                if (usuarioJson.has("contrasenha")) {
                    usuario.setContrasenha(SHA1.getHash(contrasenhaNuevaBase64));
                } else {
                    usuario.setPasswordPermanente(result.get("contrasenha").toString());
                    Contrasenha = generarPasswordAleatorio();
                    usuario.setContrasenha(SHA1.getHash(Contrasenha));
                    idUsuario = idUsuarioBase64;
                    //nombreUsuario = usuarioJson.getString("nombreUsuario");
                }
                if (usuarioJson.has("modificadoPor")) {
                    usuario.setModificadoPor(usuarioJson.getString("modificadoPor"));
                }
               // usuario.setFechaModificacion(Calendar.getInstance().getTime());
                usuario.setNombres(result.get("nombres").toString());
                usuario.setApellidos(result.get("apellidos").toString());
                usuario.setTipoDocumento(new Documento(result.get("tipoDocumento.codDocumento").toString()));

                if (result.get("sucursal.iD_sucursal") != null) {
                    usuario.setSucursal(new Sucursal(Long.parseLong(result.get("sucursal.iD_sucursal").toString())));
                }
                if (result.get("ifi.idIfi") != null) {
                    usuario.setIfi(new IFI(Integer.parseInt(result.get("ifi.idIfi").toString())));

                } else {
                    usuario.setIfi(null);
                }
                usuario.setNroDocumento(result.get("nroDocumento").toString());
                usuario.setTelefono(result.get("telefono").toString());
                usuario.setEmail(result.get("email").toString());
                usuario.setAdminifi(result.get("adminifi").toString());
                if (usuarioJson.has("cambiarpassword")) {
                    usuario.setCambiarpassword(cambiarPasswordBase64);
                }
                if (result.get("fechaUltimoIngreso") != null) {
                    usuario.setFechaUltimoIngreso((Date) result.get("fechaUltimoIngreso"));
                } else {
                    usuario.setFechaUltimoIngreso(null);
                }
                usuario.setEstado(new EstadoUsuario(result.get("estado.codEstUsu").toString()));

                List<Map<String, Object>> roles = usuarioManager.listarRoles(Integer.parseInt(result.get("idUsuario").toString()));
                Integer i = 0;
                ArrayList<Rol> rolesUsuario = new ArrayList<Rol>();
                for (Map<String, Object> rol : roles) {
                    rolesUsuario.add(new Rol(Integer.parseInt(rol.get("codRol").toString())));
                    i++;
                }
                usuario.setRoles(rolesUsuario);
            } else {
                desencriptarDatos(usuarioJson);
                usuario.setIdUsuario(Integer.parseInt(idUsuarioBase64));
                usuario.setNombreUsuario(nombreUsuarioBase64);
                Map<String, Object> result = usuarioManager.getAtributos(usuario,
                        new String[]{"idUsuario", "nombreUsuario", "nombres", "apellidos", "tipoDocumento.codDocumento", "adminifi", "cambiarpassword",
                            "ifi.idIfi", "sucursal.iD_sucursal", "nroDocumento", "telefono", "email", "fechaUltimoIngreso", "estado.codEstUsu", "passwordPermanente", "creadoPor"});
                if (result.get("creadoPor") != null) {
                    usuario.setCreadoPor(result.get("creadoPor").toString());
                }
                if (usuarioJson.has("SHA")) {
                    usuario.setContrasenha(SHA1.getHash(contrasenhaNuevaBase64));
                    if (result.get("passwordPermanente") != null) {
                        if (result.get("passwordPermanente").toString().equals(usuarioJson.getString("passwordPermanente"))) {
                            usuario.setPasswordPermanente(SHA1.getHash(usuarioJson.getString("passwordPermanente")));
                        } else {
                            usuario.setPasswordPermanente(SHA1.getHash(usuarioJson.getString("passwordPermanente")));
                        }

                    } else {
                        usuario.setPasswordPermanente(SHA1.getHash(usuarioJson.getString("passwordPermanente")));

                    }
//                    if (result.get("passwordPermanente") != null) {
//                        usuario.setPasswordPermanente(result.get("passwordPermanente").toString());
//                    } else {
//                         usuario.setPasswordPermanente(null);
//                    }
                   
                } else {
                    usuario.setContrasenha(usuarioJson.getString("contrasenha"));
                    if (result.get("passwordPermanente") != null) {
                        usuario.setPasswordPermanente(SHA1.getHash(result.get("passwordPermanente").toString()));
                    } else {
                        usuario.setPasswordPermanente(null);
                    }
                    // usuario.setEstado(new EstadoUsuario(usuarioJson.getString("estado")));
                   
                }
                usuario.setFechaModificacion(Calendar.getInstance().getTime());
                if (usuarioJson.has("modificadoPor")) {
                    usuario.setModificadoPor(usuarioJson.getString("modificadoPor"));
                }

                if (usuarioJson.has("nombres")) {
                    usuario.setNombres(nombresBase64);
                }
                if (usuarioJson.has("apellidos")) {
                    usuario.setApellidos(apellidosBase64);
                }
                if (usuarioJson.has("adminifi")) {
                    usuario.setAdminifi(adminIfiBase64);
                }
                if (usuarioJson.has("cambiarpassword")) {
                    usuario.setCambiarpassword(cambiarPasswordBase64);
                }
                if (usuarioJson.has("tipoDocumento")) {
                    usuario.setTipoDocumento(new Documento(tipoDocumentoBase64));
                }

                if (usuarioJson.has("ifi")) {
                    usuario.setIfi(new IFI(Integer.parseInt(ifiBase64)));
                }
                if (usuarioJson.has("sucursal")) {
                    usuario.setSucursal(new Sucursal(Long.parseLong(sucursalBase64)));
                }

                if (usuarioJson.has("nroDocumento")) {
                    usuario.setNroDocumento(nroDocumentoBase64);
                }
                if (usuarioJson.has("telefono")) {
                    usuario.setTelefono(telefonoBase64);
                }
                if (usuarioJson.has("email")) {
                    usuario.setEmail(emailBase64);
                }
                if (usuarioJson.has("fechaUltimoIngreso")) {
                    usuario.setFechaUltimoIngreso(dateFormat.parse(usuarioJson.getString("fechaUltimoIngreso")));
                }
                if (usuarioJson.has("estado")) {
                    usuario.setEstado(new EstadoUsuario(estadoBase64));
                }

                if (usuarioJson.has("roles")) {
                    JSONArray rolesJson = usuarioJson.getJSONArray("roles");
                    ArrayList<Rol> rolesUsuario = new ArrayList<Rol>();
                    for (int i = 0; i < rolesJson.length(); i++) {
                        rolesUsuario.add(new Rol(Integer.parseInt(rolesJson.getString(i))));
                    }
                    usuario.setRoles(rolesUsuario);
                }
            }

            usuarioManager.update(usuario, getNombreUsuario(), getEntidadFinanciera());

        } catch (Exception e) {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrió un error al modificar la Entidad: Usuario");
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(getNombreUsuario());
            sucesos.setIfi(getEntidadFinanciera());
            usuarioManager.huellaAuditoria(sucesos);
            e.printStackTrace();
            try {
                resultadoJson.put("resultado", "Error al modificar el usuario");
            } catch (JSONException ex) {
                ex.printStackTrace();
//                Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return resultadoJson.toString();
        }
        try {
            resultadoJson.put("resultado", "Usuario modificado exitosamente");
            if (Contrasenha.compareTo("null") != 0) {
                resultadoJson.put("contrasenha", Contrasenha);
                resultadoJson.put("idUsuario", idUsuario);
            }
            /*if(nombreUsuario.compareTo("null") != 0)
            {
                Usuario user = new Usuario();
                user.setNombreUsuario(nombreUsuario);
                Map<String, Object> result = usuarioManager.getAtributos(user,
                        new String[]{"idUsuario"});
                resultadoJson.put("idUsuario", result.get("idUsuario").toString());
            }*/
        } catch (JSONException ex) {
            ex.printStackTrace();
//            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultadoJson.toString();
    }

    public String eliminar(String jsonString) {
        try {
            JSONObject usuarioJson = new JSONObject(jsonString);
            //servicioManager.delete(Long.parseLong(servicioJson.getString("codigo")));
            usuarioManager.delete(Integer.parseInt(usuarioJson.getString("idUsuario")),getNombreUsuario(),getEntidadFinanciera());
            //  ciudadManager.delete(ciudadJson.getString("codCiudad"));
        } catch (Exception e) {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrió un error al eliminar la Entidad: Usuario");
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(getNombreUsuario());
            sucesos.setIfi(getEntidadFinanciera());
            usuarioManager.huellaAuditoria(sucesos);
            e.printStackTrace();
            return "Error al eliminar el usuario. " + e.getMessage();
        }
        return "Usuario eliminado correctamente";
    }

    public List<String> traerLista(String ejemploJson, Integer primerResultado, Integer cantResultados, String[] atributos) {
        try {

            Usuario ejemplo = null;
            if (ejemploJson != null && !ejemploJson.trim().isEmpty()) {
                System.out.println(ejemploJson);
                JSONObject usuarioJson = new JSONObject(ejemploJson);

                ejemplo = new Usuario();
                //ejemplo.setIdUsuario(Long.parseLong(usuarioJson.optString("idUsuario", null)));
                ejemplo.setNombreUsuario(usuarioJson.optString("nombreUsuario", null));
                //ejemplo.setContrasenha(usuarioJson.optString("contrasenha", null));

                if (usuarioJson.has("nombres")) {
                    ejemplo.setNombres(usuarioJson.optString("nombres", null));
                }
                if (usuarioJson.has("apellidos")) {
                    ejemplo.setApellidos(usuarioJson.optString("apellidos", null));
                }
                 if (usuarioJson.has("adminidi")) {
                    ejemplo.setAdminifi(usuarioJson.optString("adminifi", null));
                }
                if (usuarioJson.has("cambiarpassword")) {
                    ejemplo.setAdminifi(usuarioJson.optString("cambiarpassword", null));
                }
                if (usuarioJson.has("ifi")) {
                    IFI ifi = new IFI();
                    ifi.setIdIfi(Integer.parseInt(usuarioJson.getString("ifi")));
                    ejemplo.setIfi(ifi);
                }

                if (usuarioJson.has("tipoDocumento")) {
                    Documento docEjemplo = new Documento();
                    docEjemplo.setNombre(usuarioJson.getString("tipoDocumento"));
                    ejemplo.setTipoDocumento(docEjemplo);
                }
                if (usuarioJson.has("estado")) {
                    EstadoUsuario estUsuEjemplo = new EstadoUsuario();
                    estUsuEjemplo.setNombre(usuarioJson.getString("estado"));
                    ejemplo.setEstado(estUsuEjemplo);
                }
                  if (usuarioJson.has("sucursal")) {
                    Sucursal sucursalEjemplo = new Sucursal();
                    sucursalEjemplo.setNombre(usuarioJson.getString("sucursal"));
                    ejemplo.setSucursal(sucursalEjemplo);
                }
                if (usuarioJson.has("nroDocumento")) {
                    ejemplo.setNroDocumento(usuarioJson.optString("nroDocumento", null));
                }
                if (usuarioJson.has("telefono")) {
                    ejemplo.setTelefono(usuarioJson.optString("telefono", null));
                }
                if (usuarioJson.has("email")) {
                    ejemplo.setEmail(usuarioJson.optString("email", null));
                }
                if (usuarioJson.has("intentoFallido")) {
                    ejemplo.setIntentoFallido(Integer.parseInt(usuarioJson.getString("intentoFallido")));
                }
            }


            List<Map<String, Object>> listaAtributos =
                    usuarioManager.listAtributos(ejemplo, atributos, primerResultado, cantResultados, atributos[1], "asc", true);

            int i = 0;
            List<String> listaUsuarioString = new ArrayList<String>();
            for (Map<String, Object> mapaAtributos : listaAtributos) {
                JSONObject mapaAtributosJson = new JSONObject();

                for (String key : mapaAtributos.keySet()) {
                    mapaAtributosJson.put(key, mapaAtributos.get(key));
                }

                listaUsuarioString.add(mapaAtributosJson.toString());

            }

            //agrego al final de la lista de retorno el numero total de columnas como string
            listaUsuarioString.add(usuarioManager.total(ejemplo).toString()); //return usuarioManager.total(null);

            return listaUsuarioString;

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<String>();
        }
    }

    public Integer getTotalRows(String ejemploJson) {
        try {

            Usuario ejemplo = null;
            if (ejemploJson != null) {
                JSONObject usuarioJson = new JSONObject(ejemploJson);

                ejemplo = new Usuario();
                //ejemplo.setIdUsuario(Long.parseLong(usuarioJson.optString("idUsuario", null)));

                //if (usuarioJson.has("contrasenha")) {
                // ejemplo.setContrasenha(usuarioJson.optString("contrasenha", null));
                //}
                if (usuarioJson.has("nombreUsuario")) {
                    ejemplo.setNombreUsuario(usuarioJson.getString("nombreUsuario"));
                }
                if (usuarioJson.has("estado")) {
                    ejemplo.setEstado(new EstadoUsuario(usuarioJson.getString("estado")));
                }
                if (usuarioJson.has("sucursal")) {
                    ejemplo.setSucursal(new Sucursal(Long.parseLong(usuarioJson.getString("sucursal"))));
                }

                if (usuarioJson.has("ifi")) {
                    ejemplo.setIfi(new IFI(Integer.parseInt(usuarioJson.getString("ifi"))));
                }
                if (usuarioJson.has("adminifi")) {
                    ejemplo.setAdminifi(usuarioJson.getString("adminifi"));
                }
                if (usuarioJson.has("cambiarpassword")) {
                    ejemplo.setAdminifi(usuarioJson.getString("cambiarpassword"));
                }

                if (usuarioJson.has("nombres")) {
                    ejemplo.setNombres(usuarioJson.getString("nombres"));
                }
                if (usuarioJson.has("apellidos")) {
                    ejemplo.setApellidos(usuarioJson.getString("apellidos"));
                }
                if (usuarioJson.has("tipoDocumento")) {
                    ejemplo.setTipoDocumento(new Documento(usuarioJson.getString("tipoDocumento")));
                }
                if (usuarioJson.has("nroDocumento")) {
                    ejemplo.setNroDocumento(usuarioJson.getString("nroDocumento"));
                }
                if (usuarioJson.has("telefono")) {
                    ejemplo.setTelefono(usuarioJson.getString("telefono"));
                }
                if (usuarioJson.has("email")) {
                    ejemplo.setEmail(usuarioJson.getString("email"));
                }
                if(usuarioJson.has("intentoFallido")){
                    ejemplo.setIntentoFallido(Integer.parseInt(usuarioJson.getString("intentoFallido")));
                }
                
            }

            return usuarioManager.total(null);

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public List<String> listarRoles(Integer codigo) {
        try {
            List<Map<String, Object>> lista = usuarioManager.listarRoles(codigo);

            List<String> listaJson = new ArrayList<String>();

            for (Map<String, Object> mapa : lista) {
                JSONObject rol = new JSONObject();
                rol.put("codRol", mapa.get("codRol"));
                rol.put("nombre", mapa.get("nombre"));
                listaJson.add(rol.toString());
            }

            return listaJson;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<String>();
        }

    }

    public List<String> listarRolesUser(Integer codigo) {
        try {
            List<Map<String, Object>> lista = usuarioManager.listarRoles(codigo);

            List<String> listaJson = new ArrayList<String>();

            for (Map<String, Object> mapa : lista) {
                JSONObject rol = new JSONObject();
                rol.put("nombre", mapa.get("nombre"));
                rol.put("codRol", mapa.get("codRol"));
                rol.put("delegable", mapa.get("delegable"));
                listaJson.add(rol.toString());
            }

            List<String> listaRolJson = new ArrayList<String>();
            
            for(String rol:listaJson)
            {
//                JSONObject rolJson = new JSONObject(rol);
//                if(rolesUser.contains(rol))
//                {
                    listaRolJson.add(rol);
//                }
            }

            return listaRolJson;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<String>();
        }

    }

    public Map<String, Object> autenticar(String nombreUsuario, String contrasenha) {
        String passEncriptado = "";
        String valor = null;
        try {
            byte[] contrasenhaByte = Base64Utils.fromBase64(contrasenha);
            contrasenha = new String(contrasenhaByte,"UTF-8");
            byte[] nombreUsuarioByte = Base64Utils.fromBase64(nombreUsuario);
            nombreUsuario = new String(nombreUsuarioByte,"UTF-8");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            passEncriptado = SHA1.getHash(contrasenha);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(passEncriptado != ""){
            valor = usuarioManager.validarUsuarioPassword(nombreUsuario, passEncriptado);
        }
        
        if(valor != null){
            if(valor.equalsIgnoreCase(IGUAL)){
                try {
                    Usuario ejemplo = new Usuario();
                    EstadoUsuario estadoUsuario = new EstadoUsuario();
                    estadoUsuario.setCodEstUsu("ACT");
                    ejemplo.setEstado(estadoUsuario);
                    ejemplo.setNombreUsuario(nombreUsuario);
                    ejemplo.setContrasenha(SHA1.getHash(contrasenha));
                    Map<String, Object> result = usuarioManager.getAtributos(ejemplo,
                            new String[]{"idUsuario","nombreUsuario","contrasenha", "ifi.idIfi", "ifi.nombre", "nombres", "apellidos","nroDocumento","telefono","email","fechaUltimoIngreso",
                                "estado.codEstUsu","tipoDocumento.codDocumento","sucursal.iD_sucursal","sucursal.nombre","adminifi", "cambiarpassword",
                                "passwordPermanente", "creadoPor", "modificadoPor", "fechaModificacion"});

                    if (result != null) {
                        HttpSession sesion = this.getThreadLocalRequest().getSession();
                        sesion.setAttribute("nombreUsuario", result.get("nombres"));
                        sesion.setAttribute("idUsuario", result.get("idUsuario"));
                        sesion.setAttribute("apellidoUsuario", result.get("apellidos"));
                        Map<String, Object> permisosUsuario = usuarioManager.listaPermisos((Integer) result.get("idUsuario"));
                        sesion.setAttribute("permisoXUsuario", permisosUsuario);
                        if (result.get("ifi.idIfi") != null) {
                            sesion.setAttribute("entidadFinanciera", result.get("ifi.nombre"));
                            sesion.setAttribute("idIfi", result.get("ifi.idIfi"));
                        } else {
                            sesion.setAttribute("entidadFinanciera", "Administrador Fondo");
                            sesion.setAttribute("idIfi", null);
                        }
                        if(result.get("sucursal.iD_sucursal") != null){
                            sesion.setAttribute("iD_sucursal", Integer.parseInt(result.get("sucursal.iD_sucursal").toString()));
                        }
                        else{
                             sesion.setAttribute("iD_sucursal", null);
                        }
                        if(result.get("adminifi") != null){
                            sesion.setAttribute("adminifi", result.get("adminifi").toString());
                        }
                        //setTimeSessionExpired(10);
                        long segundos = Calendar.getInstance().getTimeInMillis() / 1000;
                        sesion.setAttribute("timeStamp", segundos);
                        //sesion.
                        //construyendo respuesta;
                        List<Map<String, Object>> listaResult = new ArrayList<Map<String, Object>>();
                        List<Map<String, Object>> rolesUsuario = usuarioManager.listarRoles((Integer) result.get("idUsuario"));
                        HashMap<String, Object> mapa = new HashMap<String, Object>();
                        mapa.put("idUsuario",result.get("idUsuario"));
                        mapa.put("userName",result.get("nombreUsuario"));
                        mapa.put("nroDocumento",result.get("nroDocumento"));
                        mapa.put("telefono",result.get("telefono"));
                        mapa.put("email",result.get("email"));
                        mapa.put("estado",result.get("estado.codEstUsu"));
                        mapa.put("tipoDocumento",result.get("tipoDocumento.codDocumento"));
                        mapa.put("usuarioNombre", sesion.getAttribute("nombreUsuario"));
                        mapa.put("apellidoUsuario", sesion.getAttribute("apellidoUsuario"));
                        if(result.get("adminifi") != null){
                              mapa.put("adminifi",result.get("adminifi"));
                        }  
                        mapa.put("entidadFinanciera", sesion.getAttribute("entidadFinanciera"));
                        mapa.put("idIFI", sesion.getAttribute("idIfi"));
                        mapa.put("iD_sucursal", sesion.getAttribute("iD_sucursal"));
                        mapa.put("permisos", sesion.getAttribute("permisoXUsuario"));
                        mapa.put("roles", rolesUsuario);
                        mapa.put("primerIngreso", "false");
                        mapa.put("cambiarpassword", result.get("cambiarpassword"));
                        mapa.put("nombreUsuario","true");
                        mapa.put("passwordUsuario","true");
                        if(result.get("creadoPor") != null){
                            mapa.put("creadoPor", result.get("creadoPor").toString());
                        }
                        if(result.get("modificadoPor") != null){
                            mapa.put("modificadoPor", result.get("modificadoPor").toString());
                        }
                        if(result.get("fechaModificacion") != null){
                            mapa.put("fechaModificacion", result.get("fechaModificacion").toString());
                        }
                        if(result.get("fechaUltimoIngreso")== null)
                        {
                            mapa.put("primerIngreso", "true");
                        }
                        else
                        {
                            mapa.put("fechaUltimoIngreso",dateFormat.format(result.get("fechaUltimoIngreso")));
                            fechaUltimoIngreso(result,rolesUsuario);
                        }
                        if(result.get("passwordPermanente") != null){
                            mapa.put("passwordPermanente",result.get("passwordPermanente"));
                        }else{
                            mapa.put("passwordPermanente",null);
                        }
                        
                        return mapa;
                    }else{
                        //En caso de que el usuario sea inactivo
                        HashMap<String, Object> mapa = new HashMap<String, Object>();
                        mapa.put("estado", "INA");
                        mapa.put("primerIngreso", "");
                        mapa.put("cambiarpassword", "");
                        mapa.put("nombreUsuario","true");
                        mapa.put("passwordUsuario","true");
                        return mapa;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(valor.equalsIgnoreCase(USUARIO)){
                    HashMap<String, Object> mapa = new HashMap<String, Object>();
                    mapa.put("estado", "");
                    mapa.put("primerIngreso", "");
                    mapa.put("cambiarpassword", "");
                    mapa.put("nombreUsuario","false");
                    mapa.put("passwordUsuario", "");
                    return mapa;
                }else{
                    if(valor.equalsIgnoreCase(PASSWORD)){
                        
                        Usuario ejemplo = new Usuario();
                        ejemplo.setNombreUsuario(nombreUsuario);
                        Map<String, Object> result = usuarioManager.getAtributos(ejemplo,
                            new String[]{"idUsuario","ifi.idIfi","adminifi","intentoFallido"});
                        HashMap<String, Object> mapa = new HashMap<String, Object>();
                        mapa.put("estado", "");
                        mapa.put("primerIngreso", "");
                        mapa.put("cambiarpassword", "");
                        mapa.put("nombreUsuario","");
                        mapa.put("passwordUsuario","false");
                        mapa.put("idUsuario",result.get("idUsuario"));
                        mapa.put("ifi.idIfi",result.get("ifi.idIfi"));
                        mapa.put("adminifi",result.get("adminifi"));
                        mapa.put("intentoFallido",result.get("intentoFallido"));
                        mapa.put("passwordPermanente",result.get("passwordPermanente"));
                        return mapa;
                    }
                }

            }
        }else{
            HashMap<String, Object> mapa = new HashMap<String, Object>();
            mapa.put("primerIngreso", "");
            mapa.put("cambiarpassword", "");
            mapa.put("nombreUsuario","");
            mapa.put("passwordUsuario","false");
            return mapa;
        }
        return new HashMap<String, Object>();

    }

    @Override
    public String processCall(String payload) throws SerializationException {
        if (payload.contains("autenticar")) {
            this.getThreadLocalRequest().getSession(true);
        }
        return super.processCall(payload);
    }

    public List<String> listar(String ejemploJson, String[] atributos, boolean like, boolean all, Integer primerResultado, Integer cantResultados, String[] orderBy, String[] direccion) throws BaseRemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<String> listar(String ejemploJson, String[] atributos, String orderBy, String direccion) throws BaseRemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<String> listarPermisos(Integer idUsuario) {
        try {
            Map<String, Object> mapaPermiso = usuarioManager.listaPermisos(idUsuario);

            List<String> listaJson = new ArrayList<String>();

            Iterator<String> keys = mapaPermiso.keySet().iterator();
            String key;
            while (keys.hasNext()) {
                key = keys.next();
                JSONObject permiso = new JSONObject();
                permiso.put(key, mapaPermiso.get(key));
                listaJson.add(permiso.toString());
            }
            return listaJson;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<String>();
        }

    }

    public Boolean controlarViolacionDeCodigo(String codigo)
     {
        try {
            Usuario usuarioEjemplo= new Usuario();
            usuarioEjemplo.setNombreUsuario(codigo);
            String [] atributos= {"nombreUsuario"};
            Boolean variable;
            Map<String, Object> mapaPermiso = usuarioManager.getAtributos(usuarioEjemplo, atributos);
            if(mapaPermiso != null)
            {
                variable=true;
            }
            else
            {
                variable=false;
            }
            
            return variable;
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public String autenticarContrasenha(String userName, String contrasenha) throws BaseRemoteException {
        try {
            byte[] userNameByte = Base64Utils.fromBase64(userName);
            userName = new String(userNameByte,"UTF-8");
            byte[] passByte = Base64Utils.fromBase64(contrasenha);
            contrasenha = new String(passByte,"UTF-8");
            Usuario ejemplo = new Usuario();
            EstadoUsuario estadoUsuario = new EstadoUsuario();
            estadoUsuario.setCodEstUsu("ACT");
            ejemplo.setEstado(estadoUsuario);
            ejemplo.setNombreUsuario(userName);
            ejemplo.setContrasenha(SHA1.getHash(contrasenha));
            Map<String, Object> result = usuarioManager.getAtributos(ejemplo,
                        new String[]{"idUsuario"});
            
            if (result != null) {
                return "true";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "false";
    }

    public String actualizarEstadoUsuario(String idUsuario, String Estado) {
        try{
            Usuario ejemplo = new Usuario();
            ejemplo.setIdUsuario(Integer.parseInt(idUsuario));
            Map<String, Object> result = usuarioManager.getAtributos(ejemplo,
                            new String[]{"idUsuario","nombreUsuario","contrasenha", "ifi.idIfi", "ifi.nombre", "nombres", "apellidos","nroDocumento","telefono","email","tipoDocumento.codDocumento","sucursal.iD_sucursal","sucursal.nombre","adminifi",
                                "cambiarpassword","intentoFallido","passwordPermanente","creadoPor","modificadoPor","fechaModificacion"});
            ejemplo.setNombreUsuario(result.get("nombreUsuario").toString());
            ejemplo.setNombres(result.get("nombres").toString());
            ejemplo.setApellidos(result.get("apellidos").toString());
            ejemplo.setAdminifi(result.get("adminifi").toString());
            ejemplo.setNroDocumento(result.get("nroDocumento").toString());
            ejemplo.setTelefono(result.get("telefono").toString());
            ejemplo.setTipoDocumento(new Documento(result.get("tipoDocumento.codDocumento").toString()));
            ejemplo.setCambiarpassword(result.get("cambiarpassword").toString());
            ejemplo.setContrasenha(result.get("contrasenha").toString());
            ejemplo.setEmail(result.get("email").toString());
            if(result.get("passwordPermanente") != null){
                ejemplo.setPasswordPermanente(result.get("passwordPermanente").toString());
            }
            ejemplo.setCreadoPor(result.get("creadoPor").toString());
            if(result.get("modificadoPor") != null){
                ejemplo.setModificadoPor(result.get("modificadoPor").toString());
            }
            ejemplo.setFechaModificacion(Calendar.getInstance().getTime());
            ejemplo.setEstado(new EstadoUsuario(Estado));
            ejemplo.setIntentoFallido(0);
            if(result.get("ifi.idIfi") != null){
                ejemplo.setIfi(new IFI(Integer.parseInt(result.get("ifi.idIfi").toString())));
                if(result.get("sucursal.iD_sucursal") != null){
                    ejemplo.setSucursal(new Sucursal(Long.parseLong(result.get("sucursal.iD_sucursal").toString())));
                }
            }else{
              ejemplo.setIfi(null);  
            }
            
            List<Map<String, Object>> rolesUsuario = usuarioManager.listarRoles(Integer.parseInt(idUsuario));

            ArrayList<Rol> roles = new ArrayList<Rol>();
            for(Map<String, Object> rol : rolesUsuario)
            {
                roles.add(new Rol(Integer.parseInt(rol.get("codRol").toString())));
            }

            ejemplo.setRoles(roles);

            usuarioManager.update(ejemplo,getNombreUsuario(),getEntidadFinanciera());

        } catch (Exception e) {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrió un error al cambiar el Estado de la Entidad: Usuario");
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(getNombreUsuario());
            sucesos.setIfi(getEntidadFinanciera());
            usuarioManager.huellaAuditoria(sucesos);
            e.printStackTrace();
            return "Error al modificar el usuario";
        }
        return "Usuario modificado exitosamente";
    }

    private  String generarPasswordAleatorio()
    {
        Random rnd = new Random(Calendar.getInstance().getTimeInMillis());
        String mayusculas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String minusculas = "abcdefghijklmnopqrstuvwxyz";
        String numeros = "0123456789";
        String password = "";
        int tamMinimoPass = 6;
        int tail = rnd.nextInt(4);
        int tamPass = tamMinimoPass + tail;
        int letraSource,posicLetra;
        // 0 mayusculas
        // 1 minusculas
        // 2 numero
        int i = 0;
        while(i < tamPass)
        {
            letraSource = rnd.nextInt(3);
            if( letraSource == 0)
            {
                posicLetra = rnd.nextInt(mayusculas.length());
                password = password + mayusculas.charAt(posicLetra);
            }
            else if( letraSource == 1)
            {
                posicLetra = rnd.nextInt(minusculas.length());
                password = password + minusculas.charAt(posicLetra);
            }
            else
            {
                posicLetra = rnd.nextInt(numeros.length());
                password = password + numeros.charAt(posicLetra);
            }
            i++;
        }
        return password;
    }

    private void fechaUltimoIngreso(Map<String, Object> result, List<Map<String, Object>> rolesUsuario) {
        try
        {
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(Integer.parseInt(result.get("idUsuario").toString()));
            usuario.setNombreUsuario(result.get("nombreUsuario").toString());
            usuario.setContrasenha(result.get("contrasenha").toString());
            usuario.setPasswordPermanente(result.get("passwordPermanente").toString());
            usuario.setNombres(result.get("nombres").toString());
            usuario.setApellidos(result.get("apellidos").toString());
            usuario.setAdminifi(result.get("adminifi").toString());
            usuario.setCambiarpassword(result.get("cambiarpassword").toString());
            usuario.setCreadoPor(result.get("creadoPor").toString());
            usuario.setModificadoPor(result.get("modificadoPor").toString());
            usuario.setFechaModificacion((Date) result.get("fechaModificacion"));
            usuario.setTipoDocumento(new Documento(result.get("tipoDocumento.codDocumento").toString()));
            if (result.get("ifi.idIfi") != null){
                usuario.setIfi(new IFI(Integer.parseInt(result.get("ifi.idIfi").toString())));
                if(result.get("sucursal.iD_sucursal") != null){
                    usuario.setSucursal(new Sucursal(Long.parseLong(result.get("sucursal.iD_sucursal").toString())));
                }}
            usuario.setNroDocumento(result.get("nroDocumento").toString());
            usuario.setTelefono(result.get("telefono").toString());
            usuario.setEmail(result.get("email").toString());
            usuario.setFechaUltimoIngreso(new Date());
            usuario.setEstado(new EstadoUsuario(result.get("estado.codEstUsu").toString()));

            ArrayList<Rol> roles = new ArrayList<Rol>();
            for(Map<String, Object> rol : rolesUsuario)
            {
                roles.add(new Rol(Integer.parseInt(rol.get("codRol").toString())));
            }
            usuario.setRoles(roles);
            usuarioManager.update(usuario,getNombreUsuario(),getEntidadFinanciera());

        } catch (Exception e)
        {
            e.printStackTrace();
            RegistroDeSucesos sucesos = new RegistroDeSucesos();
            sucesos.setCodRegSuc("ERROR");
            sucesos.setDescripcion("Ocurrió un error al modificar la Entidad: Usuario");
            sucesos.setFecha(Calendar.getInstance().getTime());
            sucesos.setUsuario(getNombreUsuario());
            sucesos.setIfi(getEntidadFinanciera());
            usuarioManager.huellaAuditoria(sucesos);
            e.printStackTrace();
        }
    }

    @Override
    public String validarUsuarioPassword(String nombre, String password) throws BaseRemoteException {
        String retorno = "";
        try {
            String encriptarPass = SHA1.getHash(password);
            retorno = usuarioManager.validarUsuarioPassword(nombre, encriptarPass);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

       public Boolean controlarViolacionDeCodigo2(String codigo) throws BaseRemoteException {
     {
        try {
            Usuario usuarioEjemplo = new Usuario();
            usuarioEjemplo.setNroDocumento(codigo);
            usuarioEjemplo.setTipoDocumento(new Documento("CI"));
            String [] atributos= {"idUsuario"};
            Boolean variable;
            Map<String, Object> mapaPermiso = usuarioManager.getAtributos(usuarioEjemplo, atributos);
            if(mapaPermiso != null)
            {
                variable=true;
            }
            else
            {
                variable=false;
            }

            return variable;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    }

    @Override
    public String guardar(String jsonString, Integer valor) throws BaseRemoteException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer cantidadIntentos(String nombreUsuario) {
        try {
           byte[] nombreUsuarioByte = Base64Utils.fromBase64(nombreUsuario);
            nombreUsuario = new String(nombreUsuarioByte,"UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        Integer respuesta = usuarioManager.cantidadIntentos(nombreUsuario);
        log.logFormateado("UsuariosServiceImpl-cantidadIntentos", "nombreUsuario: "+nombreUsuario, "Verificar la cantidad de intentos fallidos de logueo", 
                Boolean.TRUE, "Consulta exitosa",getNombreUsuario());
        return respuesta;
    }

    @Override
    public String setearCantidadIntentos(String nombreUsuario, Integer cantidad) {
          try {
            byte[] nombreUsuarioByte = Base64Utils.fromBase64(nombreUsuario);
            nombreUsuario = new String(nombreUsuarioByte, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UsuarioServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.logFormateado("UsuariosServiceImpl-setearCantidadIntentos", "nombreUsuario: "+nombreUsuario + " cantidad: "+String.valueOf(cantidad), "Modificar la cantidad de intentos fallidos de logueo", 
                Boolean.TRUE, "Modificación exitosa",getNombreUsuario());
        return usuarioManager.setearCantidadIntentos(nombreUsuario, cantidad); 
    }

    @Override
    public String intentoFallidoUsuarioInactivo(Integer idUsuario, String estado) {
        log.logFormateado("UsuariosServiceImpl-intentoFallidoUsuarioInactivo", "idUsuario: "+String.valueOf(idUsuario) + " estado: "+estado, "Modificar estado de usuario por cantidad de intentos fallidos de logueo", 
                Boolean.TRUE, "Modificación exitosa",getNombreUsuario());
        return usuarioManager.intentoFallidoUsuarioInactivo(idUsuario, estado);
    }

    @Override
    public String encriptarPasswordTemporal(String password) {
        try {
            log.logFormateado("UsuarioServiceImpl - encriptarPasswordTemporal",
                    "password", "devuelve el password encriptado",
                    Boolean.TRUE, "retorno de dato exitoso", getNombreUsuario());
            return SHA1.getHash(password);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            log.logFormateado("UsuarioServiceImpl - encriptarPasswordTemporal",
                    "password", "devuelve el password encriptado",
                    Boolean.FALSE, "retorno de dato fallido. "+ex.getMessage(), getNombreUsuario());
            return null;
        }
    }

    @Override
    public String consultaPasswordPermanente(String userName,String pass) {
        try {
            Usuario ejemplo = new Usuario();
            EstadoUsuario estadoUsuario = new EstadoUsuario();
            estadoUsuario.setCodEstUsu("ACT");
            ejemplo.setEstado(estadoUsuario);
            ejemplo.setNombreUsuario(userName);
            Map<String, Object> result = usuarioManager.getAtributos(ejemplo,
                        new String[]{"idUsuario","passwordPermanente"});
            
            if (result != null) {
                if(result.get("passwordPermanente") != null){
                    String password = SHA1.getHash(pass);
                    if(result.get("passwordPermanente").toString().compareTo(password) == 0){
                        return "true";
                    }else{
                        return "false";
                    }
                }else{
                    return "false";
                }   
            }
            log.logFormateado("UsuarioServiceImpl - consultaPasswordPermanente",
                    "UserName: "+userName+" password", "verifica si contraseña nueva coincide con la almacenada",
                    Boolean.TRUE, "retorno de dato exitoso", getNombreUsuario());
        } catch (Exception e) {
            e.printStackTrace();
            log.logFormateado("UsuarioServiceImpl - consultaPasswordPermanente",
                    "UserName: "+userName+" password", "verifica si contraseña nueva coincide con la almacenada",
                    Boolean.FALSE, "retorno de dato fallido. "+e.getMessage(), getNombreUsuario());
        }
        return "false";

    }

    @Override
    public List<Map<String, Object>> listarUsuariosPorInstitucion(Integer idUsuario) throws BaseRemoteException {
        return usuarioManager.listarUsuariosPorInstitucion(idUsuario);
    }
    
    
    public void desencriptarDatos(JSONObject usuarioJson){
        try{
        byte[] idUsuariobyte = Base64Utils.fromBase64(usuarioJson.getString("idUsuario").toString());
        idUsuarioBase64 = new String(idUsuariobyte, "UTF-8");
                if(usuarioJson.has("nombreUsuario")){
                    byte[] nombreUsuariobyte = Base64Utils.fromBase64(usuarioJson.getString("nombreUsuario").toString());
                    nombreUsuarioBase64 = new String(nombreUsuariobyte, "UTF-8");
                }  
                if (usuarioJson.has("nombres")) {
                    byte[] nombresbyte = Base64Utils.fromBase64(usuarioJson.getString("nombres").toString());
                    nombresBase64 = new String(nombresbyte,"UTF-8");
                }
                if (usuarioJson.has("apellidos")) {
                     byte[] apellidosbyte = Base64Utils.fromBase64(usuarioJson.getString("apellidos").toString());
                     apellidosBase64 = new String(apellidosbyte,"UTF-8");
                }
                if(usuarioJson.has("adminifi")){
                   byte[] apellidosbyte = Base64Utils.fromBase64(usuarioJson.getString("apellidos").toString());
                   apellidosBase64 = new String(apellidosbyte,"UTF-8");
                }
                if(usuarioJson.has("cambiarpassword")){
                   byte[] cambiarPassbyte = Base64Utils.fromBase64(usuarioJson.getString("cambiarpassword").toString());
                   cambiarPasswordBase64 = new String(cambiarPassbyte,"UTF-8");
                }
                if (usuarioJson.has("tipoDocumento")) {
                    byte[] tipoDocumentobyte = Base64Utils.fromBase64(usuarioJson.getString("tipoDocumento").toString());
                   tipoDocumentoBase64 = new String(tipoDocumentobyte,"UTF-8");
                }

                if (usuarioJson.has("ifi")) {
                   byte[] ifibyte = Base64Utils.fromBase64(usuarioJson.getString("ifi").toString());
                   ifiBase64 = new String(ifibyte,"UTF-8");
                }
                if (usuarioJson.has("sucursal")) {
                   byte[] sucursalbyte = Base64Utils.fromBase64(usuarioJson.getString("sucursal").toString());
                   sucursalBase64 = new String(sucursalbyte,"UTF-8");
                }

                if (usuarioJson.has("nroDocumento")) {
                   byte[] nroDocumentobyte = Base64Utils.fromBase64(usuarioJson.getString("nroDocumento").toString());
                   nroDocumentoBase64 = new String(nroDocumentobyte,"UTF-8");
                }
                if (usuarioJson.has("telefono")) {
                   byte[] telefonobyte = Base64Utils.fromBase64(usuarioJson.getString("telefono").toString());
                   telefonoBase64 = new String(telefonobyte,"UTF-8");
                }
                if (usuarioJson.has("email")) {
                   byte[] emailbyte = Base64Utils.fromBase64(usuarioJson.getString("email").toString());
                   emailBase64 = new String(emailbyte,"UTF-8");
                }
                if (usuarioJson.has("adminifi")) {
                   byte[] adminifibyte = Base64Utils.fromBase64(usuarioJson.getString("adminifi").toString());
                   adminIfiBase64 = new String(adminifibyte,"UTF-8");
                }
                if (usuarioJson.has("estado")) {
                   byte[] estadobyte = Base64Utils.fromBase64(usuarioJson.getString("estado").toString());
                   estadoBase64 = new String(estadobyte,"UTF-8");
                }
                 if (usuarioJson.has("contrasenha")) {
                   byte[] contrasenhabyte = Base64Utils.fromBase64(usuarioJson.getString("contrasenha").toString());
                   contrasenhaNuevaBase64 = new String(contrasenhabyte,"UTF-8");
                }
                 if(usuarioJson.has("SHA")){
                   byte[] shabyte = Base64Utils.fromBase64(usuarioJson.getString("SHA").toString());
                   shaBase64 = new String(shabyte,"UTF-8");
                 }
        }catch(Exception e){
              log.logFormateado("desencriptarDatos", usuarioJson.toString(), "Decifra los datos recibidos", 
                Boolean.TRUE, "Operación exitosa",getNombreUsuario());
        }
        
    }
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/styles.css"/>">

	</head>
	<body class="login">
		<div class="valign-wrapper">
			<form class="valign" id="reg" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
				<div class="row">
					<div class="col m6">
				    <div class="row">
						  <div class="col m6">
						    <div class="input-field">
						      <i class="icon icon-user-1 prefix"></i>
						      <label for="j_username" class="active">Usuario</label>
						      <input type="text" id="j_username" name="j_username">
						    </div>
						  </div>
						  <div class="col m6">
						    <div class="input-field">
						      <i class="icon icon-key prefix"></i>
						      <label for="j_password" class="active">Contrase&ntilde;a</label>
						      <input type="password" id="j_password" name="j_password">
						    </div>
						  </div>
							<c:if test="${error}">
				      <div class="col m12 center">
				        <span class="black-text text-darken-2 center"><i class="red-text text-darken-2 fa fa-warning"></i> <strong>${mensaje}</strong></span>
				      </div>
							</c:if>
				    </div>
				  </div>
					<div class="col m3">
						<button type="submit" class="waves-effect waves-light btn-large white-text red darken-2">Ingresar <i class="icon icon-signin"></i></button>
					</div>
				</div>
			</form>
		</div>		
			
		<div class="fondo"></div>
		<script>
		</script>
	</body>
</html>
